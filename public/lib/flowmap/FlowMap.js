/**
 * Created by Jiayi Xu on 15/1/15.
 */

function FlowMap(flowData, nodes, width, height, flowWidth, container, flowMapService) {

    flowMapService.setHours = setHours;

    var flowMap = container.append('g')
        .attr("class", "flowMap")
        .style("isolation", "isolate");

    var line = d3.svg.line()
        .x(function(d) {
            return d.X;
        })
        .y(function(d) {
            return d.Y;
        })
        .interpolate("cardinal");

    var pointAPos = flowMapService.data.pointA.pos;
    var pointBPos = flowMapService.data.pointB.pos;

    var maxVolume = 0;

    for(var i = 0; i < 2; ++i) {
        for(var j = 0; j < 24; ++j) {
            if(!flowData[i].hasOwnProperty(j)) {
                continue ;
            }

            var cluNum = flowMapService.data.clusterNum;
            for(var k = 0; k < cluNum; ++k) {
                var nodesNum = nodes[k].length;
                for(var L = 0; L < nodesNum; ++L) {
                    if (!flowData[i][j].hasOwnProperty(nodes[k][L].nid)) {
                        continue;
                    }

                    if (flowData[i][j][nodes[k][L].nid] > maxVolume) {
                        maxVolume = flowData[i][j][nodes[k][L].nid];
                    }
                }
            }
        }
    }

    var cluNum = nodes.length;
    for(var ci = 0; ci < cluNum; ++ci) {
//        if(ci != 1) {
//            continue ;
//        }

        var _nodes = nodes[ci];
        for(var ni = 0; ni < _nodes.length; ++ni) {
            var d = _nodes[ni];
            var _x = d.pX;
            var _y = d.pY;

            var innerRatioBarCenterPos = flowMapService.data.innerRatioBarCenterPos[d.cid];
            var timeBarPos = flowMapService.data.timeBarPos[d.cid];

            for (var t = 0; t < 24; ++t) {
                var volume0 = 0;
                if (flowData[0].hasOwnProperty(t) && flowData[0][t].hasOwnProperty(d.nid)) {
                    volume0 = flowData[0][t][d.nid];
                }
                var volume1 = 0;
                if (flowData[1].hasOwnProperty(t) && flowData[1][t].hasOwnProperty(d.nid)) {
                    volume1 = flowData[1][t][d.nid];
                }

                if (flowData[0].hasOwnProperty(t) && flowData[0][t].hasOwnProperty(d.nid)) {
                    var timePos = flowMapService.timePos(timeBarPos, t);
                    var volume = flowData[0][t][d.nid];

                    var flowDatum = [pointAPos, innerRatioBarCenterPos, timePos, {X: timeBarPos.X, Y: timeBarPos.Y+40}, {X: _x, Y: _y}];
                    flowDatum.source = 0;
                    flowDatum.target = d.nid;
                    flowDatum.opa = 0;
                    flowDatum.hour = t;
                    flowDatum.color = 'Green';
                    if (volume0 + volume1 > 0) {
                        flowDatum.opa = volume0 / (volume0 + volume1);
                    }
                    flowDatum.width = volume / maxVolume * flowWidth;
                    if(flowDatum.width < 0.1) {
                        flowDatum.width = 0.1;
                    }

                    flowMap.append("path")
                        .datum(flowDatum)
                        .attr("class", "flow")
                        .attr("stroke", function () {
                            return "Green";
                        })
                        .attr("stroke-linecap", "round")
                        .attr("stroke-width", function (d) {
                            return d.width;
                        })
                        .attr("stroke-opacity", function (d) {
                            return 1;
//                            return d.opa;
                        })
                        .attr("d", line)
                        .attr("fill", "none")
                        .style("mix-blend-mode", "Lighten")
                    ;
                }

                if (flowData[1].hasOwnProperty(t) && flowData[1][t].hasOwnProperty(d.nid)) {
                    var timePos = flowMapService.timePos(timeBarPos, t);
                    var volume = flowData[1][t][d.nid];

                    var flowDatum = [pointBPos, innerRatioBarCenterPos, timePos, {X: timeBarPos.X, Y: timeBarPos.Y+40}, {X: _x, Y: _y}];
                    flowDatum.source = 1;
                    flowDatum.target = d.nid;
                    flowDatum.opa = 0;
                    flowDatum.hour = t;
                    flowDatum.color = 'Crimson';
                    if (volume0 + volume1 > 0) {
                        flowDatum.opa = volume1 / (volume0 + volume1);
                    }
                    flowDatum.width = volume / maxVolume * flowWidth;
                    if(flowDatum.width < 0.1) {
                        flowDatum.width = 0.1;
                    }

                    flowMap.append("path")
                        .datum(flowDatum)
                        .attr("class", "flow")
                        .attr("stroke", function () {
                            return "Crimson";
                        })
                        .attr("stroke-linecap", "round")
                        .attr("stroke-width", function (d) {
                            return d.width;
                        })
                        .attr("stroke-opacity", function () {
                            return 1;
                        })
                        .attr("d", line)
                        .attr("fill", "none")
                        .style("mix-blend-mode", "Lighten")
                    ;
                }
            }
        }
    }


    function setHours(hours) {
        var flows = d3.selectAll('.flow');

        flows.each(function(d) {
            var color = 'grey';
            var opacity = 0.1;

            if(_.contains(hours, d.hour)) {
                color = d.color;
                opacity = 1;
            }

            d3
                .select(this)
                .attr('stroke', color)
                .attr('stroke-opacity', opacity);
        });
    }

    return flowMap;
}