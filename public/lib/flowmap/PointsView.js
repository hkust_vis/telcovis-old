/**
 * Created by Jiayi Xu on 1/2/15.
 */

function PointsView(nodes, container, flowMapService, mapService) {
    var _nodes = [];
    for(var i = 0; i < nodes.length; ++i) {
        _nodes = _nodes.concat(nodes[i]);
    }

    var glyph = undefined;

    var pointsView = container.append('g');

    var _marker = undefined;

    var points  = pointsView
        .selectAll(".cluster_node")
        .data(_nodes)
        .enter()
        .append("circle")
        .attr("class", "cluster_node")
        .attr("cx", function(d) {
            return d.pX;
        })
        .attr("cy", function(d) {
            return d.pY;
        })
        .attr("r", 5)
        .attr("fill", "grey")
        .on("mouseover", function(d, i) {
            reset();

            container.select(".flowMap")
                .selectAll(".flow")
                .attr("stroke-width", function () {
                    return 0.1;
                })
                .attr("stroke-opacity", function() {
                    return 0.1;
                });
            container.select(".flowMap")
                .selectAll(".flow")
                .filter(function(_d) {
                    if(_d.target == d.nid) {
                        return true;
                    }

                    return false;
                })
                .attr("stroke-width", function (d) {
                    return d.width*2;
                })
                .attr("stroke-opacity", function(d) {
                    return 1;
//                    return d.opa;
                });

            d3.select($('.voronoi_view_region')[i])
                .attr('stroke-width', 5);

            drawGlyph(d);

            mapService.map.dest.setView([d.gps.lat, d.gps.lng]);
            _marker = mapService.map.dest.addRedMarkerNid(d.nid);
        });

    points.append("title")
        .text(function(d, i) {
            return d.nid + ' ' + String(d.W);
        });
    var pointText = pointsView
        .selectAll(".node_text")
        .data(_nodes)
        .enter()
        .append("text")
        .attr("class", "node_text")
        .attr("x", function(d) {
            return d.pX + 2;
        })
        .attr("y", function(d) {
            return d.pY;
        })
        .text(function(d, i) {
            return String(d.W);
        });

    function reset() {
        d3.selectAll('.voronoi_view_region')
            .attr('stroke-width', 1);

        if(_marker) {
            mapService.map.dest.removeLayer(_marker);
            _marker = undefined;
        }

        if(glyph != undefined) {
            glyph.remove();
        }
    }

    function drawGlyph(d) {
        var radius = 30;

        glyph = pointsView.append("g")
            .attr("transform", "translate(" + d.pX + ", " + d.pY +")");

        glyph.append("circle")
            .attr("r", function() {
                return radius * 1.6;
            })
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", function() {
                return 4.3;
//                return radius*0.2;
            })
            .attr("stroke-dasharray", "5 5")
            .attr("stroke-dashoffset", "0");


        var angle_A = (Math.floor(d['direction_A']/90.0)+1) * Math.PI/2;
        var angle_B = (Math.floor(d['direction_B']/90.0)+1) * Math.PI/2;

        drawArcs(angle_A, d['distance_A'], "A");
        drawArcs(angle_B, d['distance_B'], "B");

        if(angle_A == angle_B) {
            drawArcs(angle_A, Math.min(d['distance_A'], d['distance_B']), "M");
        }

        function drawArcs(angle, distance, flag) {
//            var color = "rgb(222,128,50)";
//            var color = "DarkOrange";
                var color = "rgb(215,202,153)";
//            DarkOrange
            if(flag == "A") {
                color = flowMapService.data.flowColors[0];
            } else if(flag == "B") {
                color = flowMapService.data.flowColors[1];
            }

            if (distance > 10) {
                drawArc(glyph, radius * 1.1, angle, color);
            }

            if (distance > 3) {
                drawArc(glyph, radius * 1.3, angle, color);
            }

            drawArc(glyph, radius * 1.5, angle, color);

            function drawArc(container, radius, angle, color) {
                container.append('path')
                    .attr("fill", color)
                    .attr('d', d3.svg.arc().innerRadius(radius).outerRadius(radius * 1.12).startAngle(angle).endAngle(angle + Math.PI / 2));
            }
        }

        glyph.append("circle")
            .attr("r", radius)
            .attr("fill", "white")
            .attr("stroke", "black")
            .attr("stroke-width", function() {
                return radius*0.1;
            })
            .on("mouseout", function() {
                reset();

                container.select(".flowMap")
                    .selectAll(".flow")
                    .attr("stroke-width", function (_d) {
                        return _d.width;
                    })
                    .attr("stroke-opacity", function(_d) {
                        return 1;
//                        return _d.opa;
                    });
            })
            .on("click", function() {
                // filter time similar nodes
                flowMapService.data.timeFilterNodes[d.cid] = d;
                flowMapService.updateClusters();
                flowMapService.drawMainGraph();
            })
            .append("title")
            .text(function() {
                return d.nid + ' ' + String(d.W);
            });


//        var status = d['status'];
//
//        var points = [];
//        points.push({X: -radius*0.8, Y: 0});
//        var startPos = -radius*0.6;
//        points.push({X: startPos, Y: 0});
//        var step = radius*0.4;
//        for(var i = 0; i < 3; ++i) {
//            points.push({X: startPos+step*0.5, Y: -status[i]*radius*0.5});
//            points.push({X: startPos+step, Y: 0});
//            startPos = startPos + step;
//        }
//        points.push({X: radius*0.8, Y: 0});
//
//        var line = d3.svg.line()
//            .x(function(d) {
//                return d.X;
//            })
//            .y(function(d) {
//                return d.Y;
//            })
//            .interpolate("linear");
//
//        var colors = d3.scale.category10()
//            .domain(d3.range(10));
//
//        glyph.append("path")
//            .datum(points)
//            .attr("d", line)
//            .attr("fill", "none")
//            .attr("stroke", colors(5))
//            .attr("stroke-width", 1);
    }

    return pointsView;
}