/**
 * Created by Jiayi Xu on 12/1/15.
 */

function VoronoiRegionView(cid, nodes, weights, className, pos, size, voronoiIteration, container, mainGraph, flowMapService, funcService) {

    var width = size[0];
    var height = size[1];


    var offset = [pos[0], pos[1]];

    var graphGroup = container.append('g')
        .attr("class", className);
//        .attr("transform", "translate(" + pos[0] + "," + pos[1] + ")");

    setTimeBar(offset);
//    drawTimeBar();
    var timeBar = {
        drawTimeBar: drawTimeBar
    };
    flowMapService.data.timeBar.push(timeBar);

//    container.append("text")
//        .attr("transform", "translate(" + (offset[0] + width/2 - poiName.length/2*6) + ", " + (offset[1]+200) +")")
//        .text(poiName);

    var voronoiView = graphGroup.append('g');
//        .attr("transform", "translate(0, " + timeBarHeight * 1.3 + ")");

//    var voronoi = d3.geom.voronoi()
//        .x(function(d) {
//            return d.X;
//        })
//        .y(function(d) {
//            return d.Y;
//        })
//        .clipExtent([[0, 0], [width, height]]);
//    var polygons = voronoi(nodes);

    if(nodes.length == 0) {
        return voronoiView;
    }

    var polygons = getCartogram(nodes, weights);

    var oScale = d3.scale.linear()
        .domain([0, 3, 10, 30])
        .range([0, 1, 2, 3]);

    var voronoiPolygons = voronoiView.append('g')
        .selectAll("path")
        .data(polygons)
        .enter()
        .append("path")
        .attr('class', 'voronoi_view_region')
        .attr("transform", "translate(" + offset[0] + ", " + offset[1] +")")
        .attr("d", funcService.polygon)
//        .attr("fill", "grey")
        .attr("fill", function(d, i) {
            var _i = oScale(nodes[i].distance_M);
//            console.log(_i);
//            console.log(flowMapService.data.clusterColorsSequential);
            return flowMapService.data.clusterColorsSequential[cid][Math.floor(_i)];
        })
//        .attr("opacity", function(d,i){
//            return oScale(nodes[i].distance_M);
//        })
        .attr("stroke", "grey")
        .attr("stroke-width", 1);


//    var polygonPoints = voronoiView.append('g');    // draw points of polygons
//    for(var i  = 0; i < polygons.length; ++i) {
//        polygonPoints
//            .selectAll(".polygon_"+i)
//            .data(polygons[i])
//            .enter()
//            .append("circle")
//            .attr("class", "polygon_"+i)
//            .attr("transform", "translate(" + offset[0] + ", " + offset[1] +")")
//            .attr("r", 1)
//            .attr("cx", function(d) {
//                return d[0];
//            })
//            .attr("cy", function(d) {
//                return d[1];
//            })
//            .attr("fill", "black")
//            .attr("stroke", "black")
//            .append("title")
//            .text(function(d) {
//                return d.vid;
//            });
//    }

    for(var i = 0; i < nodes.length; ++i) {
        nodes[i].pX = nodes[i].X + offset[0];
        nodes[i].pY = nodes[i].Y + offset[1];
    }

    function setTimeBar(offset) {
//        drawTimeBarOld(graphGroup, offset);

        var length = height / 1.5;
        var angle = Math.PI/3;
        var timeBarHeight = (1-Math.cos(angle))/Math.sin(angle)*length;
        var timeBarWidth = height / 2 * 0.2;
//        var timeBarPos = {X: offset[0]+width/2, Y: offset[1]+200};
        var timeBarPos = {X: offset[0] + width/2, Y: offset[1]+80};
        flowMapService.data.timeBarPos.push(timeBarPos);

        var cornerHeight = Math.cos(angle)*timeBarHeight/(1-Math.cos(angle));

        var rHeight = height / 2 * 0.25;

        flowMapService.timePos = function(timeBarPos, hour) {

            var _angle = getTimeAngle(hour);
            var cornersPos = [
                {X: (timeBarPos.X-Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)},
                {X: (timeBarPos.X+Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)}
            ];
            var centerPos = timeBarPos;
            var radius = timeBarHeight + timeBarWidth/2;
            if(hour < 6) {
                centerPos = cornersPos[0];
                radius = cornerHeight - timeBarWidth/2;
            } else if(hour > 18) {
                centerPos = cornersPos[1];
                radius = cornerHeight - timeBarWidth/2;
            }

            var pos = {X: centerPos.X+radius*Math.cos(_angle), Y: centerPos.Y+radius*Math.sin(_angle)};

            return pos;
        };


        flowMapService.timePos3 = function(timeBarPos, hour) {

            var _angle = getTimeAngle(hour);
            var cornersPos = [
                {X: (timeBarPos.X-Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)},
                {X: (timeBarPos.X+Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)}
            ];
            var centerPos = timeBarPos;
            var radius = timeBarHeight-timeBarWidth/2+rHeight;
            if(hour < 6) {
                centerPos = cornersPos[0];
                radius = cornerHeight + timeBarWidth/2 - rHeight;
            } else if(hour > 18) {
                centerPos = cornersPos[1];
                radius = cornerHeight + timeBarWidth/2 - rHeight;
            }

            var pos = {X: centerPos.X+radius*Math.cos(_angle), Y: centerPos.Y+radius*Math.sin(_angle)};

            return pos;
        };

        offset[1] = offset[1] + height / 2 * 1.3 + 80;
    }

    function getTimeAngle(hour) {
        var tAngle;
        var angle = Math.PI/3;
        var _angle = Math.PI/2-angle;
        if(hour < 6) {
            tAngle = Math.PI/2 - angle/6*hour;
        } else if(hour>=6 && hour<=18) {
            tAngle = Math.PI+_angle+2*angle/12*(hour-6);
        } else if(hour > 18) {
            tAngle = Math.PI/2+angle-angle/6*(hour-18);
        }

        return tAngle;
    }

    function drawTimeBar() {
        var graphGroup = mainGraph.append('g')
            .attr("class", "time_bar")
            .style("isolation", "isolate");

        var length = height / 1.5;
        var angle = Math.PI/3;
        var timeBarHeight = (1-Math.cos(angle))/Math.sin(angle)*length;
        var timeBarWidth = height / 2 * 0.2;
        var timeBarPos = flowMapService.data.timeBarPos[cid];

        var cornerHeight = Math.cos(angle)*timeBarHeight/(1-Math.cos(angle));
        var rHeight = height / 2 * 0.25;

        var cornersPos = [
            {X: (timeBarPos.X-Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)},
            {X: (timeBarPos.X+Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)}
        ];

        var e = 0.01;   // eliminate gap in the corner

        graphGroup
            .append('path')
            .attr("transform", "translate(" + timeBarPos.X + ", " + timeBarPos.Y +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(timeBarHeight-timeBarWidth/2).outerRadius(timeBarHeight+timeBarWidth/2).startAngle(-angle).endAngle(angle));

        graphGroup
            .append('path')
            .attr("transform", "translate(" + cornersPos[0].X + ", " + cornersPos[0].Y +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(cornerHeight-timeBarWidth/2).outerRadius(cornerHeight+timeBarWidth/2).startAngle(Math.PI-angle-e).endAngle(Math.PI));

        graphGroup
            .append('path')
            .attr("transform", "translate(" + cornersPos[1].X  + ", " + cornersPos[1].Y +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(cornerHeight-timeBarWidth/2).outerRadius(cornerHeight+timeBarWidth/2).startAngle(Math.PI).endAngle(Math.PI+angle+e));

        for(var i = 0; i < 9; ++i) {
            var hour = i*3;

            var _pos = flowMapService.timePos3(timeBarPos, hour);

            var _angle = getTimeAngle(hour);
            var rWidth = 3;

            if(hour < 6) {
                _angle = _angle-Math.PI/2;
            } else if(hour>=6 && hour<=18) {
                _angle = _angle+Math.PI/2;
            } else if(hour > 18) {
                _angle = _angle-Math.PI/2;
            }

            _angle = _angle/Math.PI*180;

            graphGroup.append('rect')
                .attr("transform", "translate(" + (_pos.X-rWidth/2) + ", " + _pos.Y +"), rotate(" + _angle + " " + rWidth/2 + "," +0+ ")")
                .attr("fill", "black")
                .attr("width", rWidth)
                .attr("height", rHeight);
        }
    }

    function drawTimeBarOld(graphGroup, offset) {
        var timeBarHeight = height / 2;
        var timeBarWidth = height / 2 * 0.2;

        var timeBarPos = {X: offset[0] + width/2, Y: offset[1]+80};
        flowMapService.data.timeBarPos.push(timeBarPos);

        var timeBar = graphGroup
            .append('path')
            .attr("transform", "translate(" + (offset[0] + width / 2) + ", " + (offset[1] + 80) +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(timeBarHeight-timeBarWidth/2).outerRadius(timeBarHeight+timeBarWidth/2).startAngle(-Math.PI / 2).endAngle(Math.PI / 2));


        flowMapService.timePos = function(timeBarPos, hour) {
            var angle = (hour / 24 - 1) * Math.PI;
            var pos = {X: timeBarPos.X+timeBarHeight*1.1*Math.cos(angle), Y: timeBarPos.Y+timeBarHeight*1.1*Math.sin(angle)};

            return pos;
        };
        flowMapService.timePos2 = function(timeBarPos, hour) {
            var angle = (hour / 24 - 1) * Math.PI;
            var pos = {X: timeBarPos.X+timeBarHeight*0.9*Math.cos(angle), Y: timeBarPos.Y+timeBarHeight*0.9*Math.sin(angle)};

            return pos;
        };
        flowMapService.timePos3 = function(timeBarPos, hour) {
            var angle = (hour / 24 - 1) * Math.PI;
            var pos = {X: timeBarPos.X+timeBarHeight*1.15*Math.cos(angle), Y: timeBarPos.Y+timeBarHeight*1.15*Math.sin(angle)};

            return pos;
        };

        for(var i = 0; i < 7; ++i) {
            var _pos = flowMapService.timePos3(timeBarPos, 4*i);

            var angle = (1/6 * (i-3)) * 180;
            var rWidth = 3;
            var rHeight = height / 2 * 0.25;

            graphGroup.append('rect')
                .attr("transform", "translate(" + (_pos.X-rWidth/2) + ", " + _pos.Y +"), rotate(" + angle + " " + rWidth/2 + "," +0+ ")")
                .attr("fill", "black")
                .attr("width", rWidth)
                .attr("height", rHeight);
        }


        offset[1] = offset[1] + timeBarHeight * 1.3;
    }

    function getCartogram(nodes, weights) {
        var voronoi = d3.geom.voronoi()
            .x(function(d) {
                return d.X;
            })
            .y(function(d) {
                return d.Y;
            })
            .clipExtent([[0, 0], [width, height]]);
        var polygons = voronoi(nodes);

//        addFramePointsToOuterPolygons(polygons);
//        addFramePointsToOuterPolygons(polygons);

        var vertexes = getVertexesOfPolygons(polygons);
        markOuterVertexes(vertexes);

//        adjustTriangleOfPolygons(polygons, vertexes);



//        console.log(polygons);

        var initTotalWeight = 0;
        for(var i = 0; i < weights.length; ++i) {
            initTotalWeight = initTotalWeight + (weights[i]+1); // weight = weight+1 to avoid 0 problem
        }
        var initWeightRatio = [];
        for(var i = 0; i < weights.length; ++i) {
            initWeightRatio.push((weights[i]+1)/initTotalWeight); // weight = weight+1 to avoid 0 problem
        }

//        console.log(vertexes);

        setCentroids(nodes, polygons);
//        var iterationTimes = 500;   // control overall layout quality
//        for(var i = 0; i < iterationTimes; ++i) {
//            if(!adjustPolygons(nodes, polygons, vertexes, initWeightRatio, true)) {
//                console.log("iteration times are " + i);
//                break ;
//            }
//        }

        var iterationTimes = voronoiIteration;
        for (var i = 0; i < iterationTimes/2; ++i) {
            if (!adjustPolygons(nodes, polygons, vertexes, initWeightRatio)) {
                console.log("iteration times are " + i);
                break;
            }
        }

        addFramePointsToOuterPolygons(polygons);
        var vertexes = getVertexesOfPolygons(polygons);

        var maxMove = 1024;
        for(var k = 0; k < 20; ++k) {
            var _iterationTimes = iterationTimes/80;
            for (var i = 0; i < _iterationTimes; ++i) {
                if (!adjustPolygons(nodes, polygons, vertexes, initWeightRatio)) {
                    console.log("iteration times are " + i);
                    break;
                }
            }
            _iterationTimes = iterationTimes/80;
            for (var i = 0; i < _iterationTimes; ++i) {
                if (!adjustPolygons(nodes, polygons, vertexes, initWeightRatio, maxMove)) {
                    console.log("iteration times are " + i);
                    break;
                }
            }

            maxMove = maxMove / 2;
        }

        setVertexesInBound(vertexes);
        setCentroids(nodes, polygons);
//        console.log(nodes);

        return polygons;
    }

    function printNodesPosition(nodes) {
        var _nodesP = [];
        for(var i = 0; i < nodes.length; ++i) {
            _nodesP.push(nodes[i].X, nodes[i].Y);
        }
        console.log(_nodesP);
    }

    function getVertexesOfPolygons(polygons) {
        var vertexes = [];
        var e = 1e-6;

        for(var i = 0; i < polygons.length; ++i) {
            var polygon = polygons[i];
            for(var j = 0; j < polygon.length; ++j) {
                var indicator = true;
                for(var k = 0; k < vertexes.length; ++k) {
                    if(funcService.getDistance(polygon[j], vertexes[k]) < e) {
                        polygon[j] = vertexes[k];
                        indicator = false;
                        break;
                    }
                }
                if(indicator) {
                    addVertex(polygon[j], vertexes, false);
                }
//                    if((polygon[j][0] == 0 || polygon[j][0] == width) && (polygon[j][1] == 0 || polygon[j][1] == height)) {
//                        polygon[j].isPined = true;
//                    }
            }
        }


        var isExpandAtFirst = true;
        if(isExpandAtFirst) {
            var e = 5;
            for (var i = 0; i < vertexes.length; ++i) {
                if (vertexes[i][0] == 0) {
                    vertexes[i][0] = vertexes[i][0] - width / e;
                }

                if (vertexes[i][0] == width) {
                    vertexes[i][0] = vertexes[i][0] + width / e;
                }

                if (vertexes[i][1] == 0) {
                    vertexes[i][1] = vertexes[i][1] - height / e;
                }

                if (vertexes[i][1] == height) {
                    vertexes[i][1] = vertexes[i][1] + height / e;
                }
            }
        }

        return vertexes;
    }

    function addVertex(vertex, vertexes, isPined) {
        vertexes.push(vertex);
        vertex.vid = vertexes.length-1;
        vertex.isPined = isPined;
    }

    function markOuterVertexes(vertexes) {
        for(var i = 0; i < vertexes.length; ++i) {
            var vertex = vertexes[i];
            vertex.isOnOuterFrame = false;
            if(isOnOuterFrame(vertex)) {
                vertex.isOnOuterFrame = true;
            }
        }

    }

    function addFramePointsToOuterPolygons(polygons) {
        var n = polygons.length;

        for(var i = 0; i < n; ++i) {
            var polygon = polygons[i];
            var _polygon = [];

            for (var j = 0; j < polygon.length; ++j) {
                var p1, p2;

                if (j == 0) {
                    p1 = polygon[polygon.length - 1];
                } else {
                    p1 = polygon[j - 1];
                }

                p2 = polygon[j];

                if (p1.isOnOuterFrame && p2.isOnOuterFrame) {
                    var newP = funcService.getCenterPoint(p1, p2);
                    newP.isOnOuterFrame = true;

                    _polygon.push(newP);
                }
                _polygon.push(polygon[j]);
            }

            polygons[i] = _polygon;
        }
    }

    function isOnOuterFrame(p) {
        if((p[0] == 0) || (p[0] == width) || (p[1] == 0) || (p[1] == height)){
            return true;
        }
        return false;
    }

    function adjustTriangleOfPolygons(polygons, vertexes) {
        var n = polygons.length;

        for(var i = 0; i < n; ++i) {
            var polygon = polygons[i];
            if(polygon.length > 3) {
                continue ;
            }

            var p1 = polygon[2];
            var p2 = polygon[0];
            var len = funcService.getDistance(p1, p2);
            var index = 0;

            for(var j = 0; j < 2; ++j) {
                var _len = funcService.getDistance(polygon[j], polygon[j+1]);
                if(_len > len) {
                    p1 = polygon[j];
                    p2 = polygon[j+1];
                    len = _len;
                    index = j+1;
                }
            }

            var _vertex = [(p1[0]+p2[0])/2, (p1[1]+p2[1])/2];

            polygon.splice(index, 0, _vertex);  // insert vertex into polygon
            addVertex(_vertex, vertexes, false);

            var v1 = p2.vid;
            var v2 = p1.vid;

            for(var j = 0; j < n; ++j) {
                if(i == j) {
                    continue ;
                }

                var _polygon = polygons[j];
                if(_polygon[_polygon.length-1].vid == v1 && _polygon[0].vid == v2) {
                    _polygon.splice(0, 0, _vertex);
                    continue ;
                }

                for(var k = 0; k < _polygon.length-1; ++k) {
                    if(_polygon[k].vid == v1 && _polygon[k+1].vid == v2) {
                        _polygon.splice(k+1, 0, _vertex);
                        break ;
                    }
                }
            }

        }
    }

    function insertVertexToPolygon(polygon, index, vertex) {
        polygon.splice(index, 0, vertex);
    }

    function adjustPolygons2(nodes, polygons, vertexes, weightRatio) {
        var num = nodes.length;

        var e = 1e9; // control each step, the magnitude of change of vertex

        var areas = calculateAreas(polygons);

        var totalArea = width * height;

        var areaRatio = [];
        for (var i = 0; i < areas.length; ++i) {
            areaRatio.push(areas[i] / totalArea);
        }

        var polygonAttrs = [];
        var meanSizeError = 0;

        for(var i = 0; i < num; ++i) {
            polygonAttrs.push({});

            polygonAttrs[i]["desired"] = totalArea*weightRatio[i];
            polygonAttrs[i]["area"] = areas[i];
            polygonAttrs[i]["radius"] = Math.sqrt(Math.abs(areas[i]) / Math.PI);
            polygonAttrs[i]["desired_radius"] = Math.sqrt(polygonAttrs[i]["desired"] / Math.PI);
            polygonAttrs[i]["mass"] = polygonAttrs[i]["desired_radius"] - polygonAttrs[i]["radius"];

            polygonAttrs[i]["sizeError"] = 0;   // if areas[i] == 0, then this polygon needs to adjust, it is "broken", so did not affect other points
            if(areas[i] > 0 && polygonAttrs[i]["desired"] > 0) {
                polygonAttrs[i]["sizeError"] = Math.max(areas[i], polygonAttrs[i]["desired"]) / Math.min(areas[i], polygonAttrs[i]["desired"]);
            }

            meanSizeError = meanSizeError + polygonAttrs[i]["sizeError"];
        }
        meanSizeError = meanSizeError / num;
        var forceReductionFactor = 1/(1 + meanSizeError);

        var vectors = [];
        var maxLen = 0;

        for(var i = 0; i < vertexes.length; ++i) {
            var vector = [0, 0];
            if(vertexes[i].isPined) {
                vectors.push(vector);
                continue ;
            }

            for(var j = 0; j < num; ++j) {
                var dis = funcService.getDistance(vertexes[i], [nodes[j].X, nodes[j].Y]);
                var mass = polygonAttrs[j]["mass"];// * Math.sqrt(polygonAttrs[j]["sizeError"]);
                if(polygonAttrs[j]["radius"] == 0) { // if areas[i] == 0, then this polygon needs to adjust, it is "broken", so did not affect other points
                    mass = 0;
                }
                var radius = polygonAttrs[j]["radius"];

                var f = 0;
                if(dis != 0) {
//                    f = mass * (radius / dis);
                    f = mass*Math.PI * (1 / dis);
                }

//                var f = 0;
//                if(dis > radius) {
//                    f = mass * (radius / dis); // equal to mass * cos(thita), the small area will converge to one point
//                } else {
//                    f = mass * (Math.pow(dis,2)/Math.pow(radius,2)) * (4 - 3*(dis/radius));
//                }

                vector[0] = vector[0] + (vertexes[i][0] - nodes[j].X)*f;
                vector[1] = vector[1] + (vertexes[i][1] - nodes[j].Y)*f;
            }
            vector[0] = vector[0]*forceReductionFactor;
            vector[1] = vector[1]*forceReductionFactor;
            vectors.push(vector);

            var len = Math.sqrt(Math.pow(vector[0], 2)+Math.pow(vector[1], 2));
            if(len > maxLen) {
                maxLen = len;
            }



            if(len > 0) {
                var _polygons = funcService.objectClone(polygons);
                var _vertexes = funcService.objectClone(vertexes);

                if(len > e) {
                    _vertexes[i][0] = _vertexes[i][0] + vector[0]/len*e;
                    _vertexes[i][1] = _vertexes[i][1] + vector[1]/len*e;
//                setVertexInBound(_vertexes[i]);
                } else {
                    _vertexes[i][0] = _vertexes[i][0] + vector[0];
                    _vertexes[i][1] = _vertexes[i][1] + vector[1];
                }





                for(var k = 0; k < _polygons.length; ++k) {
                    var _polygon = _polygons[k];
                    for(var j = 0; j < _polygon.length; ++j) {
                        var _vertex = _polygon[j];
                        _vertex[0] = _vertexes[_vertex.vid][0];
                        _vertex[0] = _vertexes[_vertex.vid][1];
                    }
                }

                if (calculateObjectiveFunction(_polygons, weightRatio) < calculateObjectiveFunction(polygons, weightRatio)) {

                    vertexes[i][0] = vertexes[i][0] + vector[0];
                    vertexes[i][1] = vertexes[i][1] + vector[1];
//                    setVertexInBound(vertexes[i]);

                    setCentroids(nodes, polygons);
                }
            }
        }

////        if(maxLen > e) {
//            for(var i = 0; i < vertexes.length; ++i) {
//                var vector = vectors[i];
//
//                vector[0] = vector[0] / maxLen * e;
//                vector[1] = vector[1] / maxLen * e;
//            }
////        }
//
//
//
//        var _polygons = funcService.objectClone(polygons);
//        var _vertexes = funcService.objectClone(vertexes);
//
//        for(var i = 0; i < _vertexes.length; ++i) {
//            var vector = vectors[i];
//
//            _vertexes[i][0] = _vertexes[i][0] + vector[0];
//            _vertexes[i][1] = _vertexes[i][1] + vector[1];
////            setVertexInBound(_vertexes[i]);
//        }
//        setVertexesInBound(_vertexes);
//        for(var i = 0; i < _polygons.length; ++i) {
//            var _polygon = _polygons[i];
//            for(var j = 0; j < _polygon.length; ++j) {
//                var _vertex = _polygon[j];
//                _vertex[0] = _vertexes[_vertex.vid][0];
//                _vertex[0] = _vertexes[_vertex.vid][1];
//            }
//        }
//
//
//        if(calculateObjectiveFunction(_polygons, weightRatio) < calculateObjectiveFunction(polygons, weightRatio)) {
//            for(var i = 0; i < vertexes.length; ++i) {
//                var vector = vectors[i];
//
//                vertexes[i][0] = vertexes[i][0] + vector[0];
//                vertexes[i][1] = vertexes[i][1] + vector[1];
////                setVertexInBound(vertexes[i]);
//            }
//            setVertexesInBound(vertexes);
//        }

//        console.log(vertexes);
//        console.log(" ");

//        adjustPolygonSToConvex(polygons, polygonAttrs, vertexes, e);

//        setCentroids(nodes, polygons);

        setVertexesInBound(vertexes);

    }



    function adjustPolygons(nodes, polygons, vertexes, weightRatio, maxMove) {

        if(maxMove == undefined) {
            maxMove = 1e9;
        }

        var num = nodes.length;

        var areas = calculateAreas(polygons);

//        var totalArea = width * height;
        var totalArea = 0;
        for (var i = 0; i < areas.length; ++i) {
            totalArea = totalArea + areas[i];
        }

        var areaRatio = [];
        for (var i = 0; i < areas.length; ++i) {
            areaRatio.push(areas[i] / totalArea);
        }

        var polygonAttrs = [];
        var meanSizeError = 0;

        for(var i = 0; i < num; ++i) {
            polygonAttrs.push({});

            polygonAttrs[i]["desired"] = totalArea*weightRatio[i];
            polygonAttrs[i]["area"] = areas[i];
            polygonAttrs[i]["radius"] = Math.sqrt(Math.abs(areas[i]) / Math.PI);
            polygonAttrs[i]["desired_radius"] = Math.sqrt(polygonAttrs[i]["desired"] / Math.PI);
            polygonAttrs[i]["mass"] = polygonAttrs[i]["desired_radius"] - polygonAttrs[i]["radius"];

            polygonAttrs[i]["sizeError"] = 1e6;
            if(areas[i] > 0 && polygonAttrs[i]["desired"] > 0) {
                polygonAttrs[i]["sizeError"] = Math.max(areas[i], polygonAttrs[i]["desired"]) / Math.min(areas[i], polygonAttrs[i]["desired"]);
            }

            meanSizeError = meanSizeError + polygonAttrs[i]["sizeError"];
        }
        meanSizeError = meanSizeError / num;
        var forceReductionFactor = 1/(1 + meanSizeError);

        var vectors = [];
        var maxLen = 0;

        for(var i = 0; i < vertexes.length; ++i) {
            var vector = [0, 0];

            if(vertexes[i].isPined) {
                vectors.push(vector);
                continue ;
            }

            var affectedPolygons = [];

            for(var j = 0; j < num; ++j) {
                var polygon = polygons[j];
                var isPart = false; // is the vectex part of this polygon
                for(var k = 0; k < polygon.length; ++k) {
                    if(polygon[k].vid == i) {
                        isPart = true;
                        break;
                    }
                }

                var dis = funcService.getDistance(vertexes[i], [nodes[j].X, nodes[j].Y]);
                var mass = polygonAttrs[j]["mass"];// * Math.sqrt(polygonAttrs[j]["sizeError"]);
//                var _mass = polygonAttrs[j]["desired"] - dis;
//                if(Math.abs(_mass) > Math.abs(mass)) {
//                    mass = _mass;
//                }

                if(polygonAttrs[j]["radius"] == 0) { // if areas[i] == 0, then this polygon needs to adjust, it is "broken", so did not affect other points
                    mass = 0;
                }
                var radius = polygonAttrs[j]["radius"];

                var f = 0;
                if(dis != 0) {
//                    f = mass * (radius / dis);
                    f = mass * (1 / dis);
                }

                if(!isPart) {
//                    f = f / 50;
                    f = 0;
                }


//                var f = 0;
//                if(dis > radius) {
//                    f = mass * (radius / dis); // equal to mass * cos(thita), the small area will converge to one point
//                } else {
//                    f = mass * (Math.pow(dis,2)/Math.pow(radius,2)) * (4 - 3*(dis/radius));
//                }

                var pVertexID = -1; // whether this vertex belongs to this polygon
                for (var k = 0; k < polygon.length; ++k) {
                    var pVertex = polygon[k];
                    if (pVertex.vid == i) {
                        pVertexID = k;

                        break ;
                    }
                }


                if(pVertexID > -1) {

                    affectedPolygons.push({
                        pid: j,
                        pVertexID: pVertexID
                    });
                }

                if(pVertexID > -1 && f != 0) {    // if this vertex belongs to this polygon, make sure f > 0 could enlarge the polygon and f < 0 could narrow the polygon.
                    var _polygon = funcService.objectClone(polygon);

                    var pVertex = _polygon[pVertexID];
                    pVertex[0] = pVertex[0] + (vertexes[i][0] - nodes[j].X) * f * forceReductionFactor;
                    pVertex[1] = pVertex[1] + (vertexes[i][1] - nodes[j].Y) * f * forceReductionFactor;

                    var _area = calculateArea(_polygon);
                    var area = calculateArea(polygon);

                    if (((f > 0) && (_area < area)) || ((f < 0) && (_area > area))) {   // not satisfied, let f = 0
                        f = 0;
                    }
                }

                vector[0] = vector[0] + (vertexes[i][0] - nodes[j].X)*f;
                vector[1] = vector[1] + (vertexes[i][1] - nodes[j].Y)*f;
            }
            vector[0] = vector[0]*forceReductionFactor;
            vector[1] = vector[1]*forceReductionFactor;

            if(vector[0] != 0 && vector[1] != 0) {
                var dis = funcService.getDistance([0,0], vector);
                if(dis > maxMove) {
                    vector[0] = vector[0] / dis * maxMove;
                    vector[1] = vector[1] / dis * maxMove;
                }
            }

            if(vector[0] != 0 && vector[1] != 0) {

                for (var j = 0; j < affectedPolygons.length; ++j) {
                    var e = 1.05;

                    var pid = affectedPolygons[j].pid;
                    var pVertexID = affectedPolygons[j].pVertexID;
                    var polygon = polygons[pid];

                    var _polygon = funcService.objectClone(polygon);


                    var pVertex = _polygon[pVertexID];
                    pVertex[0] = pVertex[0] + vector[0];
                    pVertex[1] = pVertex[1] + vector[1];

                    var _area = calculateArea(_polygon);
                    if (_area == 0 || (polygonAttrs[pid]["desired"] / _area > e && _area < polygonAttrs[pid]["area"])) { // control small region will not diminish
                        vector[0] = 0;
                        vector[1] = 0;

                        break;
                    }
                    else {    // control ratio of width/height
                        var dis = funcService.getDistance([nodes.X, nodes.Y], polygon[pVertexID]);
                        var ratio = 0;
                        if (dis == 0) {
                            continue;
                        }
                        ratio = polygonAttrs[pid]["radius"] / dis;

                        var _centroid = calculateCentroid(_polygon);
                        var _radius = Math.sqrt(_area / Math.PI);
                        var _dis = funcService.getDistance(_centroid, pVertex);
                        if (_dis == 0) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }
                        var _ratio = _radius / _dis;


                        if (_ratio > e && ratio > e && _ratio > ratio) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }

                        if (_ratio < 1 / e && ratio < 1 / e && _ratio < ratio) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }
                    }



                    var _n = _polygon.length;   // not allow concave polygon

                    for (var k = pVertexID - 1; k <= pVertexID + 1; ++k) {

                        var indexList = [k - 1, k, k + 1];
                        var pList = [];

                        for (var L = 0; L < 3; ++L) {
                            if (indexList[L] < 0) {
                                indexList[L] = indexList[L] + _n;
                            }

                            if (indexList[L] >= _n) {
                                indexList[L] = indexList[L] - _n;
                            }

                            pList.push([_polygon[indexList[L]][0], -1 * _polygon[indexList[L]][1]])
                        }

                        var result = crossMulti(pList[0], pList[1], pList[2]);
                        if (result < 0) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }
                    }


                    if (vector[0] == 0 && vector[1] == 0) {
                        break;
                    }
                }
            }

            if(vector[0] != 0 || vector[1] !=0) {

                var oldVertex = vertexes[i];
                var newVertex = funcService.objectClone(oldVertex);
                newVertex[0] = newVertex[0] + vector[0];
                newVertex[1] = newVertex[1] + vector[1];

                for (var j = 0; j < polygons.length; ++j) {     // vertex could not step over edge of each polygon
                    var polygon = polygons[j];
                    for (var k = 0; k < polygon.length; ++k) {
                        var k1 = k;
                        var k2 = k1 + 1;
                        if (k2 >= polygon.length) {
                            k2 = k2 - polygon.length;
                        }

                        var v1 = polygon[k1];
                        var v2 = polygon[k2];


                        if (v1.vid == i || v2.vid == i) {
                            continue;
                        }

                        var oldCrossResult = crossMulti(oldVertex, v1, v2);
                        var newCrossResult = crossMulti(newVertex, v1, v2);

                        if((oldCrossResult >= 0 && newCrossResult <= 0) || (oldCrossResult <= 0 && newCrossResult >= 0)) {
                            var v1CrossResult = crossMulti(v1, oldVertex, newVertex);
                            var v2CrossResult = crossMulti(v2, oldVertex, newVertex);

                            if((v1CrossResult >= 0 && v2CrossResult <= 0) || (v1CrossResult <= 0 && v2CrossResult >= 0)) {
                                vector[0] = 0;
                                vector[1] = 0;

                                break ;
                            }
                        }

                    }
                    if(vector[0] == 0 && vector[1] == 0) {
                        break;
                    }
                }

                // other vertexes could not step over edge linked to this vertex
                var affectedVertexes = [];
                var affectedVertexesMap = {};
                affectedVertexesMap[i] = true;
                for(var j = 0; j < affectedPolygons.length; ++j) {
                    var aPolygon = affectedPolygons[j];
                    var pid = aPolygon.pid;
                    var polygon = polygons[pid];
                    var pVertexID = aPolygon.pVertexID;

                    var v;
                    var vid = pVertexID - 1;
                    if(vid < 0) {
                        vid = vid + polygon.length;
                    }
                    vid = polygon[vid].vid;
                    if(!affectedVertexesMap.hasOwnProperty(vid)) {
                        v = vertexes[vid];
                        affectedVertexes.push(v);
                        affectedVertexesMap[vid] = true;
                    }

                    vid = pVertexID + 1;
                    if(vid >= polygon.length) {
                        vid = vid - polygon.length;
                    }
                    vid = polygon[vid].vid;
                    if(!affectedVertexesMap.hasOwnProperty(vid)) {
                        v = vertexes[vid];
                        affectedVertexes.push(v);
                        affectedVertexesMap[vid] = true;
                    }
                }

                for(var j = 0; j < affectedVertexes.length; ++j) {
                    var aV = affectedVertexes[j];

                    for(var k = 0; k < vertexes.length; ++k) {
                        var v = vertexes[k];
                        if(v.vid == i || v.vid == aV.vid) {
                            continue ;
                        }

                        var crossResult1 = crossMulti(v, oldVertex, aV);
                        var crossResult2 = crossMulti(v, aV, newVertex);
                        var crossResult3 = crossMulti(v, newVertex, oldVertex);

                        if((crossResult1 >= 0 && crossResult2 >= 0 && crossResult3 > 0) || (crossResult1 <= 0 && crossResult2 <= 0 && crossResult3 <= 0)) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break ;
                        }

                    }
                }

                var len = funcService.getDistance([0,0], vector);
                if(len > maxLen) {
                    maxLen = len;
                }
            }

            vectors.push(vector);

//            if(vector[0] != 0 && vector[1] != 0) {  // only move one vertex point one time
//                break ;
//            }
        }

        var flag = false;
        var max = 0;
        var maxI = -1;


        for(var i = 0; i < vertexes.length; ++i) {
            var vector = vectors[i];

            if(vector[0] != 0 && vector[1] != 0) {

                var _len = funcService.getDistance([0,0], vector);

                if(_len > max) {
                    max = _len;
                    maxI = i;
                }

            }
        }


        if(maxI > -1) { // only move one vertex point one time
            flag = true;

            var vector = vectors[maxI];
            vertexes[maxI][0] = vertexes[maxI][0] + vector[0];
            vertexes[maxI][1] = vertexes[maxI][1] + vector[1];
        }

//        adjustPolygonSToConvex(polygons, polygonAttrs, vertexes);

        setCentroids(nodes, polygons);

        return flag;
    }

    function calculateObjectiveFunction(polygons, weightRatio) {
        var areas = calculateAreas(polygons);

        var totalArea = width * height;

        var result = 0;

        for (var i = 0; i < areas.length; ++i) {
            var areaRatio = areas[i] / totalArea;
//            result = result + Math.pow(Math.abs(weightRatio[i] - areaRatio), 3);

            var max = Math.max(areaRatio, weightRatio[i]);
            var min = Math.min(areaRatio, weightRatio[i]);
            var _result = 1e6;

            if(min > 0) {
                _result = max / min;
            }

            result = result + _result;
        }

//        console.log(result);

        return result;
    }

//    function calculateDegreeOfThinOfPolygon(polygon, areaRatio, weightRatio) {
//        var area = calculateArea(polygon);
//        if()
//
//    }

    function setCentroids(nodes, polygons) {
        // calculate centroid of new polygons
        var num = nodes.length;

        for(var i = 0; i < num; ++i) {
            var polygon = polygons[i];
            var centroid = calculateCentroid(polygon);
            nodes[i].X = centroid[0];
            nodes[i].Y = centroid[1];
        }
    }

    function calculateCentroid(polygon) {
        var centroid = [0, 0];
        var vertexNum = polygon.length;
        for(var j = 0; j < vertexNum; ++j) {
            centroid[0] = centroid[0] + polygon[j][0];
            centroid[1] = centroid[1] + polygon[j][1];
        }
        centroid[0] = centroid[0] / vertexNum;
        centroid[1] = centroid[1] / vertexNum;

        return centroid;
    }

    function calculateAreas(polygons) {
        var areas = [];
        for(var i = 0; i < polygons.length; ++i) {
            var area = calculateArea(polygons[i]);
            areas.push(area);
        }

        return areas;
    }

    function calculateAreaHelenFormula(polygon) {
        var area = 0;

        for(var i = 1; i < polygon.length-1; ++i) {

            var p1 = polygon[0];
            var p2 = polygon[i];
            var p3 = polygon[i+1];

            var len1 = funcService.getDistance(p1, p2);
            var len2 = funcService.getDistance(p1, p3);
            var len3 = funcService.getDistance(p2, p3);

            var p = (len1 + len2 + len3) / 2;

            var _area = Math.sqrt(p * (p-len1) * (p-len2) * (p-len3));

            if(isNaN(_area)) {
                console.log("area is NaN");
                _area = 0;
            }

            area = area + _area;
        }

        return area;
    }

    function calculateArea(polygon) {
        var area = 0;
        var n = polygon.length;

//        var p1 = [-10, -10];
        for(var i = 0; i < polygon.length; ++i) {
            var i2 = i;
            var i3 = i+1;
            if(i3>=n){
                i3 = i3 - n;
            }
            var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
            var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

//            var _area = (p1[0]*p2[1]+p2[0]*p3[1]+p3[0]*p1[1]) - (p1[0]*p3[1]+p2[0]*p1[1]+p3[0]*p2[1]);
            var _area = p2[0]*p3[1] - p3[0]*p2[1];
            _area = _area * 0.5;

            area = area + _area;
        }

        if(area == 0) {
            console.log("polygon is a line!!!");
        }

        if(area < 0) {
//            console.log("polygon is concave!!!");

            area = 0;
        }

        return area;
    }

    function adjustPolygonSToConvex(polygons, polygonAttrs, vertexes) {


//        var e = Math.sqrt(width*height/polygons.length/Math.PI) / 10;  // Add disturbance, to avoid polygon become same point or in one line

        var iterationTimes = polygons.length+1; // reduce one polygon vertexes change affects other polygons

        var flag = false;
        var ite = 0;

        do {
            if(ite > iterationTimes) {
//            console.log("adjustPolygonSToConvex: iteration is over " + iterationTimes + " times !!!");
                break;
            }

            ite = ite + 1;
            flag = false;

            for(var i = 0; i < polygons.length; ++i) {
                var polygon = polygons[i];
                var isAdjust = adjustPolygonToConvex(polygon);
                if(isAdjust) {
                    flag = true;
                }
            }

//            var e = Math.sqrt(width*height / polygons.length / Math.PI);  // set minimal size when polygon becomes one line, to expand

            for(var i = 0; i < polygons.length; ++i) {
                var polygon = polygons[i];

                var e = polygonAttrs[i]["desired_radius"];
                var isAdjust = adjustPolygonIFPolygonBecomesPointOrLine(polygon, polygonAttrs[i], e);
                if(isAdjust) {
                    flag = true;
                }
            }

//            if(flag) {
//                setVertexesInBound(vertexes);
//            }
        } while(flag);
    }

    function adjustPolygonToConvex(polygon) {   // Problem: could not handle all polygon vertexes are on one point. And need iteration enough
        var n = polygon.length;
        var flag = false;
        var ite = 0;

        var isAdjust = false;

        var iterationTimes = 2; //polygon.length;    // control one polygon's structure

        do {
            if(ite > iterationTimes) {
//                console.log("adjustPolygonToConvex: iteration is over " + iterationTimes + " times !!!");
                break;
            }

            ite = ite + 1;

            flag = false;

            // reconstruct concave polygon to convex polygon
            for (var i = 0; i < n; ++i) {
                var i1 = i - 1;
                if (i1 < 0) {
                    i1 = i1 + n;
                }
                var i2 = i;
                var i3 = i + 1;
                if (i3 >= n) {
                    i3 = i3 - n;
                }
                var p1 = [polygon[i1][0], -1 * polygon[i1][1]];
                var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
                var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

                var result = crossMulti(p1, p2, p3);
                if (result < 0 && Math.abs(result) > 0.01 && !polygon[i2].isPined) {
                    flag = true;
                    isAdjust = true;

                    adjustPointToConvex(polygon, i2, p1, p2, p3);
                }
            }

        } while(flag);

        return isAdjust;
    }

    function adjustPointToConvex(polygon, index, p1, p2, p3) {
        var _p2 = [p2[0], p2[1]];

        if(p1[0] != p3[0] && p1[1] != p3[1]) {
            var k = (p1[1] - p3[1]) / (p1[0] - p3[0]);
            var t = p1[1] - p1[0] * k;

            var _k = - 1/k;
            var _t = p2[1] - p2[0] * _k;

            var _x = (_t-t) / (k-_k);
            var _y = k*_x+t;

            _p2[0] = _x;
            _p2[1] = _y;
        } else {
            if(p1[0] == p3[0]) {
                _p2[0] = p1[0];
            } else {
                if(p1[1] == p3[1]) {
                    _p2[1] = p1[1];
                }
            }
        }

        polygon[index][0] = _p2[0];
        polygon[index][1] = -1 * (_p2[1]);

//        var vector = [_p2[0]-p2[0], _p2[1]-p2[1]];
//        if(vector[0] == 0 && vector[1] == 0) { // p1, p2 and p3 on one line
//            var _vector = [p3[0]-p1[0], p3[1]-p1[1]];
//            vector = [_vector[1], -1*_vector[0]];
//        }
//
//        var _denominator = Math.sqrt(Math.pow(vector[0], 2)+Math.pow(vector[1], 2));
//        if(_denominator!=0) {
//            vector[0] = vector[0] / _denominator;
//            vector[1] = vector[1] / _denominator;
//
//            polygon[index][0] = _p2[0]+vector[0]*e;
//            polygon[index][1] = -1 * (_p2[1]+vector[1]*e);
//        } else {
//            console.log("Adjust failed!!!");
//
//            polygon[index][0] = _p2[0];
//            polygon[index][1] = -1 * (_p2[1]);
//        }
    }

    function crossMulti(p1, p2, p3) {
        return (p1[0]*p2[1]+p2[0]*p3[1]+p3[0]*p1[1]) - (p1[0]*p3[1]+p2[0]*p1[1]+p3[0]*p2[1]);
    }


    function adjustPolygonIFPolygonBecomesPointOrLine(polygon, polygonAttr, e) {
        var n = polygon.length;
        var isAdjust = false;

        var _e = e;

        if(doesPolygonBecomeOnePoint(polygon)) {    // if polygon becomes a point, reconstruct it to be a triangle
            _e = polygonAttr["desired_radius"];  // Add disturbance, to avoid polygon become same point or in one line

//                _e = 20;
            isAdjust = true;

//            console.log("polygon becomes one point");

            var i1 = 0;
            var i2 = 1;
            var i3 = 2;

            var p1 = [polygon[i1][0], -1 * polygon[i1][1]];
            var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
            var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

//                if (p1[0] == p3[0] && p1[1] == p3[1] && p1[0] == p2[0] && p1[1] == p2[1]) { // p1, p2, p3 become same point, then reconstruct triangle

            p2[0] = p2[0] - _e;
            polygon[i2][0] = p2[0];

            p3[1] = p3[1] - _e;
            polygon[i3][1] = -1 * p3[1];

            var sliceNum = n;

            var step = Math.PI*2/sliceNum;
            var angle = -Math.PI / 2;

            for(var i = 0; i < sliceNum; ++i) {
                if(polygon[i].isPined) {
                    continue ;
                }

                var p = [polygon[i][0], -1 * polygon[i][1]];
                p[0] = p[0] + _e*Math.cos(angle);
                p[1] = p[1] + _e*Math.sin(angle);
                polygon[i][0] = p[0];
                polygon[i][1] = -1 * p[1];

                angle = angle + step;
            }
        } else {
            var area = calculateArea(polygon);

            if (doesPolygonBecomeOneLine(polygon) || area == 0 || polygonAttr['desired'] / area > 3) { // if polygon becomes a line or too thin, reconstruct it to be one polygon.
                isAdjust = true;

//                console.log("polygon becomes one line");

//                for(var i = 1; i < n; ++i) {
//                    polygon[i][0] = polygon[0][0];
//                    polygon[i][1] = polygon[0][1];
//                }

                var vectors = [];

                var maxLen = 0;
                for (var i = 0; i < n - 1; ++i) {
                    for (var j = i + 1; j < n; ++j) {
                        var p = polygon[i];
                        var q = polygon[j];
                        var dis = funcService.getDistance(p, q);
                        if (dis > maxLen) {
                            maxLen = dis;
                        }
                    }
                }

                if (maxLen > 0) {
                    _e = Math.max(_e, polygonAttr["desired"] / maxLen / 2);
                }

                for (var i = 0; i < n; ++i) {
                    var i1 = i - 1;
                    if (i1 < 0) {
                        i1 = i1 + n;
                    }
                    var i2 = i;
                    var i3 = i + 1;
                    if (i3 >= n) {
                        i3 = i3 - n;
                    }
                    var p1 = [polygon[i1][0], -1 * polygon[i1][1]];
                    var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
                    var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

                    var _vector = [p3[0] - p1[0], p3[1] - p1[1]];
                    var vector = [_vector[1], -1 * _vector[0]];
                    var _denominator = Math.sqrt(Math.pow(vector[0], 2) + Math.pow(vector[1], 2));
                    if (_denominator != 0) {
                        vector[0] = vector[0] / _denominator;
                        vector[1] = vector[1] / _denominator;
                    }

                    vectors.push(vector);
                }

                for (var i = 0; i < n; ++i) {
                    if(polygon[i].isPined) {
                        continue ;
                    }

                    var p = [polygon[i][0], -1 * polygon[i][1]];

                    var vector = vectors[i];

                    p[0] = p[0] + vector[0] * _e;
                    p[1] = p[1] + vector[1] * _e;

                    polygon[i][0] = p[0];
                    polygon[i][1] = -1 * p[1];
                }
            }
        }

        return isAdjust;
    }

    function doesPolygonBecomeOnePoint(polygon) {
        var p = polygon[0];

        for(var i = 1; i < polygon.length; ++i) {
            var q = polygon[i];
            var dis = funcService.getDistance(p, q);
            if(dis > 0.01) {
                return false;
            }
        }

        return true;
    }

    function doesPolygonBecomeOneLine(polygon) {
        if(doesPolygonBecomeOnePoint(polygon)) {
            return false ;
        }

        var p = polygon[0];
        var q = polygon[1];
        for(var i = 1; i < polygon.length; ++i) {
            q = polygon[i];
            if(p[0] != q[0] || p[1] != q[1]) {
                break;
            }
        }


        var isXSame = false;
        var k = 0;
        var t = 0;
        if(p[0] == q[0]) {
            isXSame = true;
        } else {
            k = (q[1] - p[1])/(q[0] - p[0]);
            t = p[1] - p[0]*k;
        }

        for(var i = 1; i < polygon.length; ++i) {
            if(isXSame) {
                if(polygon[i][0] != p[0]) {
                    return false ;
                }
            } else {
                var result = Math.abs(k*polygon[i][0]+t-polygon[i][1]);
                if(result > 0.01) {
                    return false;
                }
            }
        }

        return true;
    }

    function setVertexesInBound(vertexes) {
//        for(var i = 0; i < vertexes.length; ++i) {
//            setVertexInBound(vertexes[i]);
////            setVertexInBoundWidthHeight(vertexes[i], -width, -height, width*2, height*2);
//        }


        var minX = 1e6;
        var minY = 1e6;
        var maxX = -1e6;
        var maxY = -1e6;

        for(var i = 0; i < vertexes.length; ++i) {
            var _pos = vertexes[i];

            if (_pos[0] < minX) {
                minX = _pos[0];
            }
            if (_pos[0] > maxX) {
                maxX = _pos[0];
            }
            if (_pos[1] < minY) {
                minY = _pos[1];
            }
            if (_pos[1] > maxY) {
                maxY = _pos[1];
            }
        }

        var x_scale = d3.scale.linear()
            .domain([minX, maxX])
            .range([0, width]);

        var y_scale = d3.scale.linear()
            .domain([minY, maxY])
            .range([0, height]);

        for(var i = 0; i < vertexes.length; ++i) {
            if(vertexes[i].isPined) {
                continue ;
            }

            vertexes[i][0] = x_scale(vertexes[i][0]);
            vertexes[i][1] = y_scale(vertexes[i][1]);
        }
    }

    function setVertexInBound(vertex) {
        vertex[0] = Math.min(width, Math.max(0, vertex[0]));
        vertex[1] = Math.min(height, Math.max(0, vertex[1]));
    }

    function setVertexInBoundWidthHeight(vertex, limitX, limitY, width, height) {
        vertex[0] = Math.min(width, Math.max(limitX, vertex[0]));
        vertex[1] = Math.min(height, Math.max(limitY, vertex[1]));
    }

    return voronoiView;
}