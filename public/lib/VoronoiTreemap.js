/**
 * Created by Jiayi on 14/3/15.
 */

var rawData = null;

function VoronoiTreemap() {

    var maxThick = 5;

    if(voronoiTreemap != null) {
        var radialGraphDOM = $("#radial_graph");
        radialGraphDOM.empty();
    } else {
        voronoiTreemap = svg.append("g").attr("id", "radial_graph");
    }
    voronoiTreemap
        .attr("transform", "translate(" + (350) + "," + (350)+")");

    var radius = 100;
    var width = (Math.sqrt(2)*radius + 1)*2;
    var height = width;

    var n = 20;
    var maxData = 288;
    var rawData = new Array(n);
    for(var i = 0; i < n; ++i) {
        rawData[i] = new Array(n);
    }

    for(var i = 0; i < n; ++i) {
        for(var j = i+1; j < n; ++j) {
            rawData[i][j] = Math.floor(Math.random()*maxData)+1;
            rawData[j][i] = rawData[i][j];
        }
    }

    for(var i = 0; i < n; ++i) {
        var max = 0;
        for(var j = 0; j < n; ++j) {
            if(rawData[i][j] > max) {
                max = rawData[i][j];
            }
        }
        rawData[i][i] = max;
    }

    var disMatrix = new Array(n);
    for(var i = 0; i < n; ++i) {
        disMatrix[i] = new Array(n);
    }
    for(var i = 0; i < n; ++i) {
        for(var j = 0; j < n; ++j) {
            disMatrix[i][j] = 1 - rawData[i][j]/maxData;
            if(i == j) {
                disMatrix[i][j] = 0;
            }
        }
    }

    var deltaLen = width/2 - radius;

    var points = mds.classic(disMatrix);

    for(var i in points) {
        points[i][0] = (points[i][0] + 1)/2*(radius*2) + deltaLen;
        points[i][1] = (points[i][1] + 1)/2*(radius*2) + deltaLen;
    }

    var root = {
        "id": "root",
        "children": []
    };

    for(var i = 0; i < points.length; ++i) {
        var mds_point = points[i];
        var node = {
            "id": i,
            "size": rawData[i][i],
            "mds_point": mds_point
        };
        root.children.push(node);
    }
    var nodes = root.children;

    var treemap = d3.layout.voronoiTreemap().iterations(100);
    treemap
        .polygon(regularPolygon(120, width, height, 0))
        .value(function(d) {
            return d.size;
        });
    var polygons = treemap(root);

    voronoiTreemap.selectAll(".voronoi_treemap_polygon")
        .data(polygons)
        .enter()
        .append("path")
        .attr("class", "voronoi_treemap_polygon")
        .attr("d", function(d) {
            if(d.polygon) {

                return service.funcs.polygon(d.polygon);
            } else {
                console.log("Unsuccessful!! to generate polygon!!");
                return "";
            }
        })
        .attr("fill", "none")
        .attr("stroke", "black");

    drawPoints();

    function drawPoints() {
        voronoiTreemap.selectAll(".voronoi_treemap_point").remove();
        voronoiTreemap.selectAll(".voronoi_treemap_point")
            .data(nodes)
            .enter()
            .append("circle")
            .attr("class", "voronoi_treemap_point")
            .attr("r", function () {
                return 10;
            })
            .attr("cx", function (d) {
                return d.point[0];
            })
            .attr("cy", function (d) {
                return d.point[1];
            })
            .attr("fill", "black")
            .attr("stroke", "black")
            .on("mouseover", function (d, i) {
                drawMST(i);
            })
            .on("mouseout", function() {
                voronoiTreemap.selectAll(".voronoi_treemap_mst").remove();
                voronoiTreemap.selectAll(".voronoi_treemap_point")
                    .attr("fill", "black");
            });
    }



    function regularPolygon(n, w, h, p) {
        w *= .5;
        h *= .5;
        var θ = 2 * Math.PI / n,
            polygon = d3.range(n).map(function(i) {
                return [w + (w - p) * Math.sin(i * θ), h + (h - p) * -Math.cos(i * θ)];
            });
        polygon.push(polygon[0]);

        return polygon;
    }

    function drawMST(index) {

        var _nodes = [index];
        var _nodesSet = {};
        _nodesSet[index] = index;

        var edges = [];
        for(var i in nodes) {
            if(i == index || rawData[index][i] == 0) {
                continue ;
            }

            var min = 1e9;
            var minIndexFrom = -1;
            var minIndexTo = -1;
            for(var j in _nodesSet) {
                if(disMatrix[i][j] < min) {
                    min = disMatrix[i][j];
                    minIndexFrom = j;
                    minIndexTo = i;
                }
            }

            edges.push([minIndexFrom, minIndexTo]);
            _nodes.push(minIndexTo);
            _nodesSet[minIndexTo] = minIndexTo;
        }

        voronoiTreemap.selectAll(".voronoi_treemap_mst").remove();
        voronoiTreemap.selectAll(".voronoi_treemap_mst")
            .data(edges)
            .enter()
            .append("path")
            .attr("class", "voronoi_treemap_mst")
            .attr("d", function(d) {

                var startPoint = [nodes[d[0]].point[0], nodes[d[0]].point[1]];
                var endPoint = [nodes[d[1]].point[0], nodes[d[1]].point[1]];

                var v1 = rawData[index][d[0]];
                var v2 = rawData[index][d[1]];

                return line_thickTOthin(startPoint, v1/maxData*maxThick, endPoint, v2/maxData*maxThick);
            })
            .attr("fill", "black")
            .attr("stroke", "black");

        drawPoints();
        d3.select(voronoiTreemap.selectAll(".voronoi_treemap_point")[0][index])
            .attr("fill", "red");
    }

    function line_thickTOthin(p1, v1, p2, v2) {

        var len = service.funcs.getDistance(p1, p2);
        if(len == 0) {
            return [p1, p2];
        }

        var points = [];

        var vec = [(p2[0]-p1[0])/len, (p2[1]-p1[1])/len];

        var _vec = [-vec[1], vec[0]];
        var _p = service.funcs.objectClone(p1);
        _p[0] = _p[0] + _vec[0]*v1/2;
        _p[1] = _p[1] + _vec[1]*v1/2;
        points.push(_p);

        _vec = [vec[1], -vec[0]];
        _p = service.funcs.objectClone(p1);
        _p[0] = _p[0] + _vec[0]*v1/2;
        _p[1] = _p[1] + _vec[1]*v1/2;
        points.push(_p);



        vec = [(p1[0]-p2[0])/len, (p1[1]-p2[1])/len];

        _vec = [-vec[1], vec[0]];
        _p = service.funcs.objectClone(p2);
        _p[0] = _p[0] + _vec[0]*v2/2;
        _p[1] = _p[1] + _vec[1]*v2/2;
        points.push(_p);

        _vec = [vec[1], -vec[0]];
        _p = service.funcs.objectClone(p2);
        _p[0] = _p[0] + _vec[0]*v2/2;
        _p[1] = _p[1] + _vec[1]*v2/2;
        points.push(_p);

        return service.funcs.polygon(points);
    }
}
