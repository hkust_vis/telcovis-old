function Horizon() {
  var mode = "offset",
      buffer = document.createElement("canvas"),
      width = 200, // will adjust latter
      height = 40,
      scale = d3.scale.linear().interpolate(d3.interpolateRound),
      extent = [0, 800],
      title = "",
      format = d3.format(".2s"),
    //  colors = ["#08519c","#3182bd","#6baed6","#bdd7e7","#bae4b3","#74c476","#31a354","#006d2c"];
    colors = ['rgb(230,245,208)','rgb(184,225,134)','rgb(127,188,65)','rgb(77,146,33)','rgb(199,234,229)','rgb(128,205,193)','rgb(53,151,143)','rgb(1,102,94)'];



    //  var _values = [1, 2, -3, -4, -5, 2, 7, 8, 9, 1, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0];

  function horizon(selection, _values, _height) {
      height = _height-1;

      var values = [];
      var step = Math.ceil(width / _values.length) - 1;

      if(_values.length > 1) {

          var _scale = d3.scale.linear().interpolate(d3.interpolateNumber);

          values.push(_values[0]);
          for(var i = 1; i < _values.length; ++i) {
              _scale
                  .domain([0, step-1])
                  .range([_values[i-1], _values[i]]);
              for(var j = 0; j < step; ++j) {
                  values.push(_scale(j));
              }
              values.push(_values[i]);
          }
      } else {
          values = _values;
      }

      width = values.length;
    selection.style("height", height+"px")
        .style("width", width+"px");

    selection.append("canvas")
        .attr("width", width)
        .attr("height", height);

    selection.append("span")
        .attr("class", "title")
        .text(title);

    selection.append("span")
        .attr("class", "value");

    selection.each(function(d, i) {
      var that = this,
          id = 0,
          canvas = d3.select(that).select("canvas"),
          span = d3.select(that).select(".value");

      canvas.datum({id: id});
      canvas = canvas.node().getContext("2d");

        var ready;
        var m = colors.length >> 1;

        var metricOverlap = 6;

        var cellSize = width / values.length;


        function change() {
            canvas.save();

            // compute the new extent and ready flag
            ready = extent.every(isFinite);

            // if this is an update (with no extent change), copy old values!
            var i0 = 0;
            var max = Math.max(-extent[0], extent[1]);

            // update the domain
            scale.domain([0, max]);

            // clear for the new data
            canvas.clearRect(0, 0, width, height);

            canvas.fillStyle = "#FAFAFA";
            canvas.fillRect(0, 0, width, height);

            // record whether there are negative values to display
            var negative;

            // positive bands
            for (var j = 0; j < m; ++j) {
                canvas.fillStyle = colors[m + j];

                // Adjust the range based on the current band index.
                var y0 = (j - m + 1) * height;
                scale.range([m * height + y0, y0]);
                y0 = scale(0);

                for (var i = 0, n = values.length, y1; i < n; ++i) {
                    y1 = values[i];
                    if (y1 <= 0) { negative = true; continue; }
                    if (y1 === undefined) continue;
                    canvas.fillRect(i*cellSize, y1 = scale(y1), cellSize, y0 - y1);
                }
            }

            if (negative) {
                // enable offset mode
                if (mode === "offset") {
                    canvas.translate(0, height);
                    canvas.scale(1, -1);
                }

                // negative bands
                for (var j = 0; j < m; ++j) {
                    canvas.fillStyle = colors[m - 1 - j];

                    // Adjust the range based on the current band index.
                    var y0 = (j - m + 1) * height;
                    scale.range([m * height + y0, y0]);
                    y0 = scale(0);

                    for (var i = 0, n = values.length, y1; i < n; ++i) {
                        y1 = values[i];
                        if (y1 >= 0) continue;
                        canvas.fillRect(i*cellSize, scale(-y1), cellSize, y0 - scale(-y1));
                    }
                }
            }

            canvas.restore();
        }


//      function focus(i) {
//        if (i == null) i = values.length - 1;
//        var value = values[i];
//        span.datum(value).text(isNaN(value) ? null : format);
//      }

//        selection
//            .on("mousemove.horizon", function() { focus(Math.round(d3.mouse(this)[0])); })
//            .on("mouseout.horizon", function() {  focus(null); });

      change();
//      focus();
    });
  }

  horizon.remove = function(selection) {

//    selection
//        .on("mousemove.horizon", null)
//        .on("mouseout.horizon", null);

    selection.selectAll("canvas")
        .each(remove)
        .remove();

    selection.selectAll(".title,.value")
        .remove();

    function remove(d) {
      d.metric.on("change.horizon-" + d.id, null);
    }
  };

  horizon.height = function(_) {
    if (!arguments.length) return height;
    height = +_;
    return horizon;
  };

  horizon.extent = function(_) {
    if (!arguments.length) return extent;
    extent = _;
    return horizon;
  };

  horizon.title = function(_) {
    if (!arguments.length) return title;
    title = _;
    return horizon;
  };

  horizon.format = function(_) {
    if (!arguments.length) return format;
    format = _;
    return horizon;
  };

  horizon.colors = function(_) {
    if (!arguments.length) return colors;
    colors = _;
    return horizon;
  };

  return horizon;
}
