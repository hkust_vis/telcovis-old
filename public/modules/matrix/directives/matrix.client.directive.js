'use strict';

angular.module('matrix').directive('matrixDirective', ["matrixService", 'configService', 'pipService', function(matrixService, configService, pipService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var config = configService.matrixGraph;

            var isLegend = true;

            var tooltip = drawTooltip();

            matrixService.switchIsLegend = switchIsLegend;
            matrixService.constructMatrixGraph = constructMatrixGraph;
            matrixService.clear = clear;

            matrixService.loadData();

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_matrix")
                    .style("display", isLegend ? "block" : "none");
            }

            function drawTooltip() {
                var tooltip = d3.select("body")
                    .append("div")
                    .attr("id", "tooltip")
                    .attr("class", "tooltip right")
                    .style("position", "absolute")
                    .style("z-index", "10")
                    .style("line-height", "20px")
                    .style("visibility", "hidden");

                tooltip
                    .append("div")
                    .attr("class", "tooltip-arrow");

                return tooltip;
            }

            function clear() {
                $("#matrix_graph").empty();
                $("#legend_matrix").empty();
            }

            function constructMatrixGraph(data) {
                matrixService.setSelectedSource = setSelectedSource;
                matrixService.setSelectedDest = setSelectedDest;
                matrixService.filterMatrixCell = filterMatrixCell;

                clear();
                var matrixSVG = d3.select("#matrix_graph").append("svg");

                var width = config.size.width;
                var height = config.size.height;

                var cellSize = Math.min(width/data.max_n, height/data.max_m);
                var cellGap = config.cell.gap;

                var selectedCell = {};
                var selectedSource = d3.set();
                var selectedDest = d3.set();

                var isClick = true;

                // x and y scale of matrix cells
                var x = d3.scale.linear()
                    .domain([1, data.max_n])
                    .range([0, (cellSize+cellGap)*(data.max_n-1)]);

                var y = d3.scale.linear()
                    .domain([1, data.max_m])
                    .range([0, (cellSize+cellGap)*(data.max_m-1)]);

                var zoom = d3.behavior.zoom()
                    .x(x)
                    .y(y)
                    .scaleExtent(config.zoomScaleExtent)
                    .on("zoom", zoomed);

                matrixSVG
                    .attr("width", width)
                    .attr("height", height)
                    .call(zoom);


                var contentMessage = tooltip
                    .append("div")
                    .attr("class", "tooltip-inner").
                    style("text-align", "left");

                var dataList = _.map(data.matrix, function(v, m_n) {
                    var m = +m_n.split('_')[0];
                    selectedCell[m] = d3.set();
                    var n = +m_n.split('_')[1];

                    return {
                        "m":m,
                        "n":n,
                        "frequency":v.m_n_num,
                        "source": v.source,
                        "destination": v.destination,
                        selected: false
                    };
                });

                var maxFrequency = d3.max(dataList, function(d) {
                    return d.frequency;
                });

                // opacity scale of cells
                var oScale = d3.scale.linear()
                    .domain([0, Math.log(maxFrequency)])
                    .range(config.cell.opacityRange);

                var matrixCellGroup = matrixSVG.append("g")
                    .attr("class", "matrix_cell_group");

                drawLegend();
                drawMatrix();

                function zoomed() {
                    var scale = zoom.scale();

                    matrixCellGroup.selectAll(".matrix_cell")
                        .attr("width", cellSize*scale)
                        .attr("height", cellSize*scale)
                        .attr("x", function (d) {
                            return x(d.n);
                        })
                        .attr("y", function (d) {
                            return y(d.m);
                        });
                }

                function drawLegend() {
                    var legendConfig = config.legend;

                    var margin = legendConfig.margin;
                    var legendWidth = d3.select("#legend_matrix")[0][0].offsetWidth;
                    var legendHeight = d3.select("#legend_matrix")[0][0].offsetHeight;
                    var legend = d3.select("#legend_matrix")
                        .append("svg")
                        .attr("id", "legendOfMatrix")
                        .attr("width", legendWidth)
                        .attr("height", legendHeight);

                    var gradient = legend.append("g").append("svg:defs")
                        .append("svg:linearGradient")
                        .attr("id", "gradient1")
                        .attr("x1", "0%")
                        .attr("y1", "0%")
                        .attr("x2", "100%")
                        .attr("y2", "0%")
                        .attr("spreadMethod", "pad");

                    gradient.append("svg:stop")
                        .attr("offset", "0%")
                        .attr("stop-color", config.cell.color.normal)
                        .attr("stop-opacity", legendConfig.startOpacity);

                    gradient.append("svg:stop")
                        .attr("offset", "100%")
                        .attr("stop-color", config.cell.color.normal)
                        .attr("stop-opacity", legendConfig.endOpacity);

                    legend
                        .append("g")
                        .append("rect")
                        .attr("width", legendWidth - margin.left - margin.right)
                        .attr("height", legendHeight - margin.top - margin.bottom)
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                        .attr("fill", "url(#gradient1)");

                    var textSize = legendConfig.textSize;

                    legend
                        .append("text")
                        .text("No. of biclusters")
                        .style("font-size", textSize + "px")
                        .style("font-weight", "bold")
                        .style("text-anchor", "start")
                        .attr("transform", "translate(" + margin.left + "," + (margin.top - textSize) + ")");

                    legend
                        .append("text")
                        .text("0")
                        .style("font-size", textSize + "px")
                        .style("text-anchor", "start")
                        .attr("transform", "translate(" + margin.left + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");

                    legend
                        .append("text")
                        .text(maxFrequency)
                        .style("text-anchor", "end")
                        .style("font-size", textSize + "px")
                        .attr("transform", "translate(" + (legendWidth - margin.right) + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");
                }

                function drawMatrix() {
                    var scale = zoom.scale();

                    matrixCellGroup.selectAll(".matrix_cell").remove();

                    matrixCellGroup.selectAll(".matrix_cell")
                        .data(dataList)
                        .enter()
                        .append("rect")
                        .attr("class", "matrix_cell")
                        .attr("width", cellSize*scale)
                        .attr("height", cellSize*scale)
                        .attr("x", function (d) {
                            return x(d.n);
                        })
                        .attr("y", function (d) {
                            return y(d.m);
                        })
                        .attr("fill", config.cell.color.normal)
                        .attr("opacity", function (d) {
                            return Math.max(config.cell.minOpacity, oScale(Math.log(d.frequency)));
                        })
                        .on("mouseover", mouseover)
                        .on("mouseout", mouseout)
                        .on("click", mousedown);
                }

                function mouseover(d) {
                    tooltip.style("visibility", "visible")
                        .style("left", (d3.event.pageX) + 30 + "px")
                        .style("top", (d3.event.pageY) - 30 + "px")
                        .transition()
                        .duration(200)
                        .style("opacity", config.tooltip.opacity);

                    var messageForTooltip = "m: "+ d.m.toString()+"</br>" + "n: "+ d.n.toString()+"</br>" + "No. of biclusters: "+ d.frequency.toString();
                    contentMessage.html(messageForTooltip);
                }

                function mouseout () {
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                }

                function mousedown(d) {
                    if(!isClick) {
                        return ;
                    }

                    var oldColor = this.attributes.fill.value;
                    if(oldColor == 'gray' || oldColor == 'orange') {
                        return ;
                    }

                    d.selected = !d.selected;
                    var color = config.cell.color.normal;
                    if(d.selected) {
                        color = config.cell.color.selected;
                        selectedCell[d.m].add(d.n);
                    } else {
                        color = config.cell.color.normal;
                        selectedCell[d.m].remove(d.n);
                    }
                    d3.select(this).attr("fill", color);

                    var mnList = selectedCellToMNList(selectedCell);

                    pipService.emitParallelCoordinatesDataChange({
                        mnList: mnList,
                        selectedSource: selectedSource,
                        selectedDest: selectedDest
                    })
                }

                function clearSelectedCell() {
                    for(var i in selectedCell) {
                        selectedCell[i] = d3.set();
                    }

                    matrixCellGroup
                        .selectAll(".matrix_cell")
                        .attr("fill", config.cell.color.normal);
                }

                // put selected cell to a list
                function selectedCellToMNList(selectedCell) {
                    var list = [];
                    for(var i in selectedCell) {
                        var values = selectedCell[i].values();
                        for(var j in values) {
                            list.push([+i, +values[j]]);
                        }
                    }

                    return list;
                }

                function setSelectedSource(_selectedSourceList) {
                    isClick = false;

                    var _selectedSource = d3.set();
                    for(var i in _selectedSourceList) {
                        _selectedSource.add(_selectedSourceList[i]);
                    }
                    selectedSource = _selectedSource;
                }

                function setSelectedDest(_selectedDestList) {
                    isClick = false;

                    var _selectedDest = d3.set();
                    for(var i in _selectedDestList) {
                        _selectedDest.add(_selectedDestList[i]);
                    }
                    selectedDest = _selectedDest;
                }

                function filterMatrixCell() {
                    clearSelectedCell();

                    var testList = [];

                    matrixCellGroup.selectAll(".matrix_cell")
                        .attr("fill", function(d) {
                            if(selectedSource.size() == 0 && selectedDest.size() == 0) {
                                return config.cell.color.normal;
                            }

                            var sourceCount = 0;
                            if(selectedSource.size() > 0) {
                                sourceCount = d.source.filter(function(v) {
                                    return selectedSource.has(v);
                                }).length;
                            }

                            var destCount = 0;
                            if(selectedDest.size() > 0) {
                                destCount = d.destination.filter(function(v) {
                                    return selectedDest.has(v);
                                }).length;
                            }

                            if(sourceCount == selectedSource.size() && destCount == selectedDest.size()) {
                                if(selectedSource.size() + selectedDest.size() == 1) {
                                    return config.cell.color.normal;
                                }

                                var m = d.m;
                                var n = d.n;

                                var thisDom = this;

                                testList.push({
                                    m: m,
                                    n: n,
                                    dom: thisDom
                                });

                                return config.cell.color.testing;
                            } else {
                                return config.cell.color.unavailable;
                            }
                        });

                    // deeply test cells
                    matrixService.testCellsDataDeeply(testList, selectedSource.values(), selectedDest.values(), config);

                    isClick = true;
                }
            }
        }
    }
}]);