'use strict';

angular.module('matrix').controller('matrixController', ['$scope', 'matrixService', 'dataService', 'dbService', 'pipService',
    function($scope, matrixService, dataService, dbService, pipService) {
        $scope.refresh = refresh;
        $scope.switchIsLegend = switchIsLegend;

        matrixService.loadData = loadData;
        matrixService.loadDataHours = loadDataHours;
        matrixService.testCellsDataDeeply = testCellsDataDeeply;

        pipService.onDateListChange($scope, function() {
            loadData();
        });

        pipService.onHoursChange($scope, function() {
            loadDataHours();
        });

        function refresh() {
            matrixService.filterMatrixCell();
        }

        function switchIsLegend() {
            matrixService.switchIsLegend();
        }

        function loadData() {
            matrixService.clear();

            var dateList = dataService.getDateList();

            var name = '/matrix/day';
            var paras = {
                dateList: dateList
            };

            dbService.post(name, paras, function(data) {
                matrixService.data = data;

                matrixService.constructMatrixGraph(data);
            });
        }

        function loadDataHours() {
            matrixService.clear();

            var hours = dataService.getHours();

            if(hours.length >= 24) {
                loadData();

                return ;
            }

            var dateList = dataService.getDateList();

            var name = '/matrix/hour';
            var paras = {
                dateList: dateList,
                hours: hours
            };

            dbService.post(name, paras, function(data) {
                matrixService.data = data;

                matrixService.constructMatrixGraph(data);
            });
        }

        function testCellsDataDeeply(testList, selectedSourceList, selectedDestList, config) {
            if(testList.length <= 0) {
                return ;
            }

            var dateList = dataService.getDateList();

            testCellDataDeeply(0);

            function testCellDataDeeply(index) {
                var m = testList[index].m;
                var n = testList[index].n;
                var dom = testList[index].dom;

                var name = '/matrix/test';
                var paras = {
                    dateList: dateList,
                    m_n: m+'_'+n,
                    selectedSource: selectedSourceList,
                    selectedDest: selectedDestList
                };

                dbService.post(name, paras, function(res) {
                    if(res.flag) {
                        d3.select(dom).attr("fill", config.cell.color.normal);
                    } else {
                        d3.select(dom).attr("fill", config.cell.color.unavailable);
                    }

                    if(index+1 < testList.length) {
                        testCellDataDeeply(index+1);
                    }
                });
            }
        }
    }
]);