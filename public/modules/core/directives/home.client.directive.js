'use strict';

angular.module('core').directive('homeDirective', ['homeService', 'dataService', function(homeService, dataService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var wholeDateList = dataService.getWholeDateList();

            setDateRangeSlider();
            setHourRangeSlider();

            function setDateRangeSlider() {
                $(function () {
                    $("#date-slider-range").slider({
                        range: true,
                        min: 0,
                        max: wholeDateList.length-1,
                        values: [ 0, wholeDateList.length-1 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#date-range").val(wholeDateList[start] + " - " + wholeDateList[end]);
                        }
                    });
                    $("#date-range").val(wholeDateList[$("#date-slider-range").slider("values", 0)] +
                        " - " + wholeDateList[$("#date-slider-range").slider("values", 1)]);
                });
            }

            function setHourRangeSlider() {

                $(function () {
                    $("#hour-slider-range").slider({
                        range: true,
                        min: 0,
                        max: 23,
                        values: [ 0, 23 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#hour-range").val(start + " - " + end);
                        }
                    });
                    $("#hour-range").val($("#hour-slider-range").slider("values", 0) +
                        " - " + $("#hour-slider-range").slider("values", 1));
                });
            }
        }
    }
}]);