'use strict';

angular.module('core').factory('configService',
    function() {

        var matrixGraph = {
            size: {
                width: 500,
                height: 260
            },
            cell: {
                gap: 1,
                opacityRange: [0.3, 1],
                minOpacity: 0.1,
                color: {
                    normal: 'dodgerblue',
                    selected: 'red',
                    unavailable: 'gray',
                    testing: 'orange'
                }
            },
            zoomScaleExtent: [1, 32],
            legend: {
                margin: {
                    left: 5,
                    right: 15,
                    top: 45,
                    bottom: 20
                },
                textSize: 8,
                startOpacity: 0.1,
                endOpacity: 1
            },
            tooltip: {
                opacity: 0.9
            }
        };

        var parallelCoordinates = {
            size: {
                width: 520,
                height: 360
            },
            margin: {
                top: 30,
                right: 50,
                bottom: 20,
                left: 10
            },
            dimensionNameMap: {
                "m_sd_distance":"m.Std.Dist",
                "m_mean_distance":"m.Avg.Dist",
                "n_m_sd_distance":"n.m.Std.Dist",
                "n_m_min":"n.m.Min.Dist",
                "n_m_mean_distance":"n.m.Avg.Dist",
                "n_m_max": "n.m.Max.Dist",
                "time_slice_sum":"no.TimeSlice",
                "persons_sum":"no.People"
            },
            histogram: {
                slice: {
                    num: 100,
                    gap: 10
                },
                opacity: 0.8,
                colorScale: {
                    domain: [-2, -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 2],
                    range: ['rgb(165,0,38)', 'rgb(215,48,39)', 'rgb(244,109,67)', 'rgb(253,174,97)', 'rgb(254,224,139)', 'rgb(255,255,191)', 'rgb(217,239,139)', 'rgb(166,217,106)', 'rgb(102,189,99)', 'rgb(26,152,80)', 'rgb(0,104,55)']  //red,yellow,green
                }
            },
            axis: {
                textAnchor: 'middle',
                y: -9
            },
            brush: {
                x: -8,
                width: 16
            },
            legend: {
                margin: {
                    top: 20,
                    bottom: 10,
                    left: 35,
                    right: 30
                }
            }
        };

        return {
            matrixGraph: matrixGraph,
            parallelCoordinates: parallelCoordinates
        };
    }
);
