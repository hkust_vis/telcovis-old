'use strict';

angular.module('core').factory('pipService', ['$window', '$rootScope',
	function($window, $rootScope) {

        var MAP_DATA_MESSAGE = "mapDataChange";
        var PARALLEL_COORDINATES_DATA_MESSAGE = "parallelCoordinatesDataChange";
        var LINE_UP_DATA_MESSAGE = "lineUpDataChange";
        var RADIAL_GRAPH_DATA_MESSAGE = "radialGraphDataChange";
        var FLOW_MAP_STAY_DATA_MESSAGE = "flowMapStayDataChange";
        var FLOW_MAP_SOURCE_MESSAGE = "flowMapSourceChange";
        var HOUR_RANGE_MESSAGE = 'hourRangeChange';
        var HOURS_MESSAGE = 'hoursChange';
        var DATE_LIST_MESSAGE = 'dateListChange';

		var emitMapDataChange = function(data){
			$rootScope.$broadcast(MAP_DATA_MESSAGE, data);
		};

		var onMapDataChange = function(scope, callback){
			scope.$on(MAP_DATA_MESSAGE, function(event, data){
				callback(data);
			});
		};

        var emitParallelCoordinatesDataChange = function(data){
            $rootScope.$broadcast(PARALLEL_COORDINATES_DATA_MESSAGE, data);
        };

        var onParallelCoordinatesDataChange = function(scope, callback){
            scope.$on(PARALLEL_COORDINATES_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitLineUpDataChange = function(data){
            $rootScope.$broadcast(LINE_UP_DATA_MESSAGE, data);
        };

        var onLineUpDataChange = function(scope, callback){
            scope.$on(LINE_UP_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitRadialGraphDataChange = function(data){
            $rootScope.$broadcast(RADIAL_GRAPH_DATA_MESSAGE, data);
        };

        var onRadialGraphDataChange = function(scope, callback){
            scope.$on(RADIAL_GRAPH_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };


        var emitFlowMapStayDataChange = function(data){
            $rootScope.$broadcast(FLOW_MAP_STAY_DATA_MESSAGE, data);
        };

        var onFlowMapStayDataChange = function(scope, callback){
            scope.$on(FLOW_MAP_STAY_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitFlowMapSourceChange = function(data){
            $rootScope.$broadcast(FLOW_MAP_SOURCE_MESSAGE, data);
        };

        var onFlowMapSourceChange = function(scope, callback){
            scope.$on(FLOW_MAP_SOURCE_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitHourRangeChange = function(data){
            $rootScope.$broadcast(HOUR_RANGE_MESSAGE, data);
        };

        var onHourRangeChange = function(scope, callback){
            scope.$on(HOUR_RANGE_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitHoursChange = function(data){
            $rootScope.$broadcast(HOURS_MESSAGE, data);
        };

        var onHoursChange = function(scope, callback){
            scope.$on(HOURS_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitDateListChange = function(data){
            $rootScope.$broadcast(DATE_LIST_MESSAGE, data);
        };

        var onDateListChange = function(scope, callback){
            scope.$on(DATE_LIST_MESSAGE, function(event, data){
                callback(data);
            });
        };

        // Public API
		return {
            emitMapDataChange: emitMapDataChange,
            onMapDataChange: onMapDataChange,

            emitParallelCoordinatesDataChange: emitParallelCoordinatesDataChange,
            onParallelCoordinatesDataChange: onParallelCoordinatesDataChange,

            emitLineUpDataChange: emitLineUpDataChange,
            onLineUpDataChange: onLineUpDataChange,

            emitRadialGraphDataChange: emitRadialGraphDataChange,
            onRadialGraphDataChange: onRadialGraphDataChange,

            emitFlowMapStayDataChange: emitFlowMapStayDataChange,
            onFlowMapStayDataChange: onFlowMapStayDataChange,
            emitFlowMapSourceChange: emitFlowMapSourceChange,
            onFlowMapSourceChange: onFlowMapSourceChange,

            emitHourRangeChange: emitHourRangeChange,
            onHourRangeChange: onHourRangeChange,
            emitHoursChange: emitHoursChange,
            onHoursChange: onHoursChange,

            emitDateListChange: emitDateListChange,
            onDateListChange: onDateListChange
		};
	}
]);
