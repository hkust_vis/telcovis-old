'use strict';

angular.module('core').factory('dbService', ['$http',
    function($http) {

        function get(name, callback){
            $http.get(name)
                .success(function(data, status, headers, config){
                    callback(data);
                })
                .error(function(data, status, headers, config){
                    console.log('get data from db error: ' + status);
                });
        }

        function gets(names, callback) {
            var i = 0;
            var dataList = [];

            var _callback = function(data) {
                dataList.push(data);

                if(names.length <= i) {
                    callback(dataList);
                    return ;
                }

                get(names[i++], _callback);
            };

            if(names.length > 0) {
                get(names[i++], _callback);
            } else {
                callback(dataList);
            }
        }

        function post(name, req, callback){
            $http.post(name, req)
                .success(function(data, status, headers, config){
                    callback(data);
                })
                .error(function(data, status, headers, config){
                    console.log('get data from db error: ' + status);
                });
        }

        function posts(names, reqs, callback) {
            var i = 0;
            var dataList = [];

            var _callback = function(data) {
                dataList.push(data);

                if(names.length <= i) {
                    callback(dataList);
                    return ;
                }

                post(names[i], reqs[i++], _callback);
            };

            if(names.length > 0) {
                post(names[i], reqs[i++], _callback);
            } else {
                callback(dataList);
            }
        }

        // Public API
        return {
            get: get,
            gets: gets,

            post: post,
            posts: posts
        };
    }
]);