'use strict';

angular.module('core').factory('jsonService', ['$http',
	function($http) {

		var getJson = function(path, callback){
			$http.get(path).success(
				function(res){
					callback(res);
				}
			);
		};

        var getJsons = function(paths, callback) {
            var i = 0;
            var dataList = [];

            var _callback = function(data) {
                dataList.push(data);

                if(paths.length <= i) {
                    callback(dataList);
                    return ;
                }

                getJson(paths[i++], _callback);
            };

            if(paths.length > 0) {
                getJson(paths[i++], _callback);
            } else {
                callback(dataList);
            }
        };

		// Public API
		return {
			getJson: getJson,
            getJsons: getJsons
		};
	}
]);