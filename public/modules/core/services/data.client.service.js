'use strict';

angular.module('core').factory('dataService', ['pipService', function(pipService) {
    var wholeDateList = ['20131020', '20131021'];
    var dateList = ['20131020', '20131021'];
    var hourRange = {
        start: 0,
        end: 24
    };
    var hours = d3.range(24);

    function getWholeDateList() {
        return wholeDateList;
    }

    function getDateList() {
        return dateList;
    }

    function setDateList(_dateList) {
        if(!_.isEqual(dateList, _dateList)) {
            dateList = _dateList;

            pipService.emitDateListChange(dateList);
        }
    }

    function getHourRange() {
        return hourRange;
    }

    function setHourRange(start, end) {
        start = +start;
        end = +end;

        var _start = Math.min(start, end);
        var _end = Math.max(start, end);
        if(_start != hourRange.start || _end != hourRange.end) {
            hourRange.start = _start;
            hourRange.end = _end;

            pipService.emitHourRangeChange(hourRange);
        }
    }

    function getHours() {
        return hours;
    }

    function setHours(_hours) {
        _hours.forEach(function(d) {
            d = +d;
        });

        _hours = _.uniq(_hours);

        _hours.sort(function(a, b) {
            return a-b;
        });

        if(!_.isEqual(hours, _hours)) {
            hours = _hours;

            pipService.emitHoursChange(hours);

            setHourRange(hours[0], hours[hours.length - 1]+1);
        }
    }

    return {
        getWholeDateList: getWholeDateList,

        getDateList: getDateList,
        setDateList: setDateList,

        getHourRange: getHourRange,
        setHourRange: setHourRange,
        getHours: getHours,
        setHours: setHours
    }
}]);