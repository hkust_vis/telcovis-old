'use strict';

angular.module('flowMap').controller('flowMapController', ['$scope', 'flowMapService', 'dataService', 'dbService', 'pipService', 'mapService',
    function($scope, flowMapService, dataService, dbService, pipService) {

        $scope.changeControlParameters = function changeControlParameters() {
            flowMapService.changeControlParameters();
        };

        pipService.onFlowMapSourceChange($scope, function(sources) {
            flowMapService.data.sources = sources;
            flowMapService.loadData(sources);
        });

        flowMapService.loadData = loadData;

        function loadData(sources) {
            if(!sources || sources.length == 0) {
                flowMapService.clearMainGraph();

                return ;
            }

            var name = '/flowmap';
            var paras = {
                gpsIDs: sources,
                dateList: dataService.getDateList()
            };

            dbService.post(name, paras, function(data) {
                flowMapService.data.stayData = data;

                pipService.emitFlowMapStayDataChange(flowMapService.data.stayData);
            });
        }


    }
]);