'use strict';

angular.module('parallelCoordinates').directive('parallelCoordinatesDirective', ["parallelCoordinatesService", 'configService', 'funcService', 'dataService', 'pipService', 'lineUpService', function(parallelCoordinatesService, configService, funcService, dataService, pipService, lineUpService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var config = configService.parallelCoordinates;

            var isLegend = true;
            parallelCoordinatesService.switchIsLegend = switchIsLegend;
            drawLegend();

            pipService.onDateListChange(scope, function() {
                parallelCoordinatesService.loadData([], [], []);
            });

            constructParallelCoordinates();

            function drawLegend() {
                d3.select("#legend_parallel_coordinates svg").remove();

                var legendMargin = config.legend.margin;

                var legendWidth = d3.select("#legend_parallel_coordinates")[0][0].offsetWidth;
                var legendHeight = d3.select("#legend_parallel_coordinates")[0][0].offsetHeight;
                var legend = d3
                    .select("#legend_parallel_coordinates")
                    .append("svg")
                    .attr("id", "legendOfPc")
                    .attr("width", legendWidth)
                    .attr("height", legendHeight);

                var gradient = legend.append("g").append("svg:defs")
                    .append("svg:linearGradient")
                    .attr("id", "gradient4")
                    .attr("x1", "0%")
                    .attr("y1", "0%")
                    .attr("x2", "0%")
                    .attr("y2", "100%")
                    .attr("spreadMethod", "pad");

                gradient.append("svg:stop").attr("offset", "0%").attr("stop-color", "rgb(0,104,55)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "10%").attr("stop-color", "rgb(26,152,80)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "20%").attr("stop-color", "rgb(102,189,99)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "30%").attr("stop-color", "rgb(166,217,106)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "40%").attr("stop-color", "rgb(217,239,139)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "50%").attr("stop-color", "rgb(255,255,191)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "60%").attr("stop-color", "rgb(254,224,139)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "70%").attr("stop-color", "rgb(253,174,97)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "80%").attr("stop-color", "rgb(244,109,67)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "90%").attr("stop-color", "rgb(215,48,39)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "100%").attr("stop-color", "rgb(165,0,38)").attr("stop-opacity", 1);

                legend
                    .append("g")
                    .append("rect")
                    .attr("width", legendWidth - legendMargin.left - legendMargin.right)
                    .attr("height", legendHeight - legendMargin.top - legendMargin.bottom)
                    .attr("transform", "translate(" + legendMargin.left + "," + legendMargin.top + ")")
                    .attr("fill", "url(#gradient4)");

                legend
                    .append("text")
                    .text("Z-score")
                    .style("font-size", "8px")
                    .style("font-weight", "bold")
                    .style("text-anchor", "start")
                    .attr("transform", "translate(" + (legendMargin.left + (legendWidth - legendMargin.left - legendMargin.right + 10)) + "," + (legendMargin.top) + ")rotate(90)");

                legend.append("text")
                    .text("2")
                    .style("font-size", "8px")
                    .style("text-anchor", "middle")
                    .attr("transform", "translate(" + (legendMargin.left - 10) + "," + (legendMargin.top + 10) + ")");

                legend
                    .append("text")
                    .text("0")
                    .style("font-size", "8px")
                    .style("text-anchor", "middle")
                    .attr("transform", "translate(" + (legendMargin.left - 10) + "," + (legendHeight / 2 + legendMargin.top - 10) + ")");

                legend
                    .append("text")
                    .text("-2")
                    .style("text-anchor", "middle")
                    .style("font-size", "8px")
                    .attr("transform", "translate(" + (legendMargin.left - 10) + "," + (legendHeight + legendMargin.top - 30) + ")");
            }

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_parallel_coordinates")
                    .style("display", isLegend ? "block" : "none");
            }

            function constructParallelCoordinates() {
                parallelCoordinatesService.loadData = loadData;
                parallelCoordinatesService.setData = setData;
                parallelCoordinatesService.clearGraph = clearGraph;
                parallelCoordinatesService.drawGraph = drawGraph;
                parallelCoordinatesService.setDimensions = setDimensions;
                parallelCoordinatesService.switchIsHistogram = switchIsHistogram;

                var wholeData = [];
                var data = [];
                parallelCoordinatesService.wholeData = data;
                parallelCoordinatesService.data = data;

                var svg;
                var margin = config.margin;
                var width = config.size.width;
                var height = config.size.height;

                var sliceNum = config.histogram.slice.num;
                var isHistogram = false;

                var x = d3.scale.ordinal().rangePoints([0, width], 1);
                var y = {}; // for dimensions

                var dragging = {};
                var line = d3.svg.line();
                var axis = d3.svg.axis().orient("left");
                var background; // grey lines
                var foreground; // blue lines

                var dimensions = parallelCoordinatesService.getDimensions(); // init dimension list
                var newDimensions; // ordered dimensions
                var dimensionNameMap = config.dimensionNameMap;

                pipService.onHoursChange(scope, function() {
                    setData();
                });


                function clearGraph() {  // clear graph
                    $("#parallel_coordinates").empty();

                    y = {};
                    dragging = {};
                    svg = d3.select("#parallel_coordinates").append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                }

                function loadData(mnList, selectedSource, selectedDest) {
                    clearGraph();
                    lineUpService.lineUp.setData([]);

                    if(mnList.length == 0) {
                        wholeData = [];
                        data = [];
                        parallelCoordinatesService.wholeData = wholeData;
                        parallelCoordinatesService.data = data;

                        return ;
                    }

                    lineUpService.getDataListFiltered(mnList, selectedSource.values(), selectedDest.values(), function(dataList) {
                        wholeData = dataList.reduce(function(preV, curV) {
                            return preV.concat(curV);
                        });

                        parallelCoordinatesService.wholeData = wholeData;
                        setData();
                    });
                }

                function setData() {
                    var hours = dataService.getHours();

                    data = funcService.objectClone(wholeData);

                    if(hours.length < 24) {
                        filterData(hours);
                    }

                    parallelCoordinatesService.data = data;

                    lineUpService.lineUp.setData(data);
                    drawGraph();
                }

                function filterData(hours) {
                    for (var i in data) {
                        filterMNList(hours, data[i].value.m);
                        filterMNList(hours, data[i].value.n);

                        data[i].value.info.time_slice_sum = funcService.getPropertyNum(data[i].value.m[0].time_slice_persons);
                        data[i].value.info.persons_sum = data[i].value.m.reduce(function (preV, curV) {
                            return preV + curV.day_persons;
                        }, 0);
                    }
                    data = data.filter(function (d) {
                        return d.value.info.time_slice_sum;
                    });

                    function filterMNList(hours, list) {
                        for(var i in list) {
                            list[i].day_persons = 0;

                            for(var j in list[i].time_slice_persons) {
                                var sliceTime = j;
                                if (!_.contains(hours, parseInt(sliceTime/12))) {
                                    delete list[i].time_slice_persons[j];
                                    continue ;
                                }

                                list[i].day_persons += list[i].time_slice_persons[j]
                            }
                        }
                    }
                }

                function drawGraph() {
                    clearGraph();

                    if(isDataAvailable()) {
                        return ;
                    }

                    setY();

                    var statisticDims = getStatisticOfDimensions();

                    newDimensions = orderDimensions(statisticDims);
                    x.domain(newDimensions);

                    drawBackground();
                    drawForeground();

                    var g = drawDimensionGroups();
                    drawHistogram(g, statisticDims);
                    drawAxis(g);
                    drawBrushRect(g);
                }

                function isDataAvailable() {
                    return !data || data==null || data.length==0;
                }

                function setY() {
                    for (var i in dimensions) {
                        var d = dimensions[i];
                        y[d] = d3.scale.linear()
                            .domain(d3.extent(data, function (p) {
                                return +p.value.info[d];
                            }))
                            .range([height, 0]);
                    }
                }

                function getStatisticOfDimensions() {
                    var statisticDims = {};

                    // calculate slice list related statistic
                    for (var i in dimensions) {
                        statisticDims[dimensions[i]] = {};

                        var dList = data.map(function(v) {
                            return v.value.info[dimensions[i]]
                        });

                        var mean = d3.mean(dList);
                        var sigma = d3.deviation(dList);    // standard deviation

                        statisticDims[dimensions[i]].mean = mean;
                        statisticDims[dimensions[i]].sigma = sigma;

                        for (var j in dList) {
                            dList[j] = dList[j] - mean;
                        }
                        statisticDims[dimensions[i]].deviationList = dList;
                    }

                    // calculate per slice related statistic
                    for (var i in dimensions) {
                        var domain = y[dimensions[i]].domain();
                        var min = domain[0];
                        var max = domain[1];

                        statisticDims[dimensions[i]].min = min;
                        statisticDims[dimensions[i]].max = max;

                        var _sliceNum = sliceNum;
                        var step = (max - min) / _sliceNum;
                        if (dimensions[i] == "time_slice_sum") {
                            _sliceNum = max - min + 1;
                            step = 1;
                        }

                        var dataRecordNumPerSlice = new Array(_sliceNum);
                        var dataValuePerSlice = new Array(_sliceNum);
                        for (var j = 0; j < _sliceNum; ++j) {
                            dataRecordNumPerSlice[j] = 0;
                            dataValuePerSlice[j] = 0;
                        }

                        for (var j in data) {
                            var d = data[j].value.info[dimensions[i]];

                            var did = Math.floor((d - min) / step);
                            if (d == max) {
                                did = _sliceNum - 1;
                            }
                            ++dataRecordNumPerSlice[did];
                            dataValuePerSlice[did] = dataValuePerSlice[did] + d;
                        }

                        for (var j = 0; j < _sliceNum; ++j) {
                            if (dataRecordNumPerSlice[j] > 0) {
                                dataValuePerSlice[j] = dataValuePerSlice[j] / dataRecordNumPerSlice[j];
                            }
                        }

                        statisticDims[dimensions[i]].sliceNum = _sliceNum;

                        statisticDims[dimensions[i]].dataRecordNumPerSlice = dataRecordNumPerSlice;
                        statisticDims[dimensions[i]].maxDataRecordNumPerSlice = d3.max(dataRecordNumPerSlice);

                        statisticDims[dimensions[i]].dataValuePerSlice = dataValuePerSlice;
                    }

                    return statisticDims;
                }

                function calculateDimDis(vi, vj) {   // calculate cos value of angle of two vectors
                    var ij = numeric.dot(vi, vj);
                    ij = ij*ij;
                    var ii = numeric.dot(vi, vi);
                    var jj = numeric.dot(vj, vj);

                    var dis = 0;
                    if(ij == 0 || (ii == 0 && jj == 0)) {
                        dis = 0;
                    } else {
                        dis = Math.sqrt(ij/(ii*jj));
                    }

                    return dis;
                }

                function getDimensionDistanceMatrix(statisticDims) {  // distance of every two dims
                    var dimN = dimensions.length;

                    // init array
                    var dimDisMatrix= new Array(dimN);
                    for(var i = 0; i < dimN; ++i) {
                        dimDisMatrix[i] = new Array(dimN);
                    }

                    for(var i = 0; i < dimN; ++i) {
                        dimDisMatrix[i][i] = 1;

                        var dListI = statisticDims[dimensions[i]].deviationList;
                        for(var j = i+1; j < dimN; ++j) {
                            var dListJ = statisticDims[dimensions[j]].deviationList;

                            dimDisMatrix[i][j] = calculateDimDis(dListI, dListJ);
                            dimDisMatrix[j][i] = dimDisMatrix[i][j];
                        }
                    }

                    return dimDisMatrix;
                }



                function orderDimensions(statisticDims) {
                    var dimN = dimensions.length;
                    var dimDisMatrix = getDimensionDistanceMatrix(statisticDims);

                    var max = -1;
                    var maxI = -1;
                    var maxJ = -1;
                    for(var i = 0; i < dimN; ++i) {
                        for(var j = i+1; j < dimN; ++j) {
                            if(dimDisMatrix[i][j] > max) {
                                max = dimDisMatrix[i][j];
                                maxI = i;
                                maxJ = j;
                            }
                        }
                    }

                    var dimsIndexs = [maxI, maxJ];
                    var dimSet = d3.set(d3.range(dimN));
                    dimSet.remove(maxI);
                    dimSet.remove(maxJ);

                    var lr = "L";   // Left or Right
                    var relatedIndex = 0;

                    while(dimSet.size() > 0) {
                        var max = -1;
                        var maxIndex = -1;

                        var values = dimSet.values();
                        for(var i in values) {
                            if(dimDisMatrix[dimsIndexs[relatedIndex]][values[i]] > max) {
                                max = dimDisMatrix[dimsIndexs[relatedIndex]][values[i]];
                                maxIndex = values[i];
                            }
                        }
                        maxIndex = +maxIndex;

                        if(lr == "L") {
                            dimsIndexs = [maxIndex].concat(dimsIndexs);
                            lr = "R";
                            relatedIndex = dimsIndexs.length-1;
                        } else {
                            dimsIndexs.push(maxIndex);
                            lr = "L";
                            relatedIndex = 0;
                        }

                        dimSet.remove(maxIndex);
                    }

                    var newDims = dimsIndexs.map(function(v) {
                        return dimensions[v];
                    });

                    return newDims;
                }

                function drawBackground() { // Add grey background lines for context.
                    background = svg.append("g")
                        .attr("class", "background")
                        .selectAll("path")
                        .data(data)
                        .enter().append("path")
                        .attr("opacity", 0.1)
                        .attr("d", path);

                    background.style("display", isHistogram ? "none" : "");
                }

                function drawForeground() { // Add blue foreground lines for focus.
                    var forGroundOpacity = 0.1;
                    if(data.length < 50){
                        forGroundOpacity = 0.7;
                    } else if(data.length < 150){
                        forGroundOpacity = 0.5;
                    }

                    foreground = svg.append("g")
                        .attr("class", "foreground")
                        .selectAll("path")
                        .data(data)
                        .enter().append("path")
                        .attr("opacity", forGroundOpacity)
                        .attr("d", path);

                    foreground.style("display", isHistogram ? "none" : "");
                }

                function drawDimensionGroups() {    // Add a group element for each dimension and return these groups
                    var g = svg.selectAll(".dimension")
                        .data(newDimensions)
                        .enter().append("g")
                        .attr("class", "dimension")
                        .attr("transform", function (d) {
                            return "translate(" + x(d) + ")";
                        })
                        .call(d3.behavior.drag()
                            .origin(function (d) {
                                return {
                                    x: x(d)
                                };
                            })
                            .on("dragstart", function (d) {
                                dragging[d] = x(d);
                                background.attr("visibility", "hidden");
                            })
                            .on("drag", function (d) {
                                dragging[d] = Math.min(width, Math.max(0, d3.event.x));
                                foreground.attr("d", path);
                                newDimensions.sort(function (a, b) {
                                    return position(a) - position(b);
                                });
                                x.domain(newDimensions);

                                g.attr("transform", function (d) {
                                    return "translate(" + position(d) + ")";
                                })
                            })
                            .on("dragend", function (d) {
                                delete dragging[d];

                                transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
                                transition(foreground).attr("d", path);

                                background
                                    .attr("d", path)
                                    .transition()
                                    .delay(500)
                                    .duration(0)
                                    .attr("visibility", null);
                            }));

                    return g;
                }

                function calculateZScore(value, mean, sigma) {
                    var zScore = 0;
                    if (sigma > 0) {
                        zScore = (value - mean) / sigma;
                    }

                    return zScore;
                }

                function drawHistogram(container, statisticDims) {
                    var histogramConfig = config.histogram;

                    var maxSliceLen = width / dimensions.length - histogramConfig.slice.gap;

                    var zScoreColorScale = d3.scale.linear()
                        .domain(histogramConfig.colorScale.domain)
                        .range(histogramConfig.colorScale.range)
                        .interpolate(d3.interpolateLab);

                    container.append("g")
                        .style("display", isHistogram ? "" : "none")
                        .selectAll(".slices")
                        .data(function (d) {
                            return statisticDims[d].dataRecordNumPerSlice;
                        })
                        .enter()
                        .append("rect")
                        .attr("class", "slices")
                        .attr("transform", function (d, i) {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];
                            var value = sta.dataValuePerSlice[i];

                            return "translate(" + (-0.5 * d / sta.maxDataRecordNumPerSlice * maxSliceLen) + ", " + (y[dim](value) - 0.5 * height / sta.sliceNum) + ")";
                        })
                        .attr("width", function (d) {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];

                            return d / sta.maxDataRecordNumPerSlice * maxSliceLen;
                        })
                        .attr("height", function () {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];

                            return height / sta.sliceNum;
                        })
                        .attr("fill", function (d, i) {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];
                            var value = sta.dataValuePerSlice[i];

                            var zScore = calculateZScore(value, sta.mean, sta.sigma);

                            return zScoreColorScale(zScore);
                        })
                        .attr("opacity", histogramConfig.opacity);
                }

                function drawAxis(container) {  // Add an axis and title.
                    var axisConfig = config.axis;

                    container.append("g")
                        .attr("class", "axis")
                        .each(function (d) {
                            d3
                                .select(this)
                                .call(axis.scale(y[d]));
                        })
                        .append("text")
                        .style("text-anchor", axisConfig.textAnchor)
                        .attr("y", axisConfig.y)
                        .text(function (d) {
                            return dimensionNameMap[d];
                        });
                }

                function drawBrushRect(container) {  // Add and store a brush for each axis.
                    var brushConfig = config.brush;

                    container.append("g")
                        .attr("class", "brush")
                        .each(function(d) {
                            d3
                                .select(this)
                                .call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
                        })
                        .selectAll("rect")
                        .attr("x", brushConfig.x)
                        .attr("width", brushConfig.width);
                }


                function position(d) {
                    var v = dragging[d];
                    return v == null ? x(d) : v;
                }

                function transition(g) {
                    return g.transition().duration(500);
                }

                function path(d) {  // Returns the path for a given data point.
                    return line(newDimensions.map(function(p) {
                        return [position(p), y[p](d.value.info[p])];
                    }));
                }

                function brushstart() {
                    d3.event.sourceEvent.stopPropagation();
                }

                function brush() {  // Handles a brush event, toggling the display of foreground lines.
                    var actives = dimensions.filter(function(p) {
                            return !y[p].brush.empty();
                        });
                    var extents = actives.map(function(p) {
                            return y[p].brush.extent();
                        });

                    if(isHistogram) {
                        if (actives.length == 0) {
                            background.style("display", "none");
                            foreground.style("display", "none");

                            return;
                        } else {
                            background.style("display", "");
                            foreground.style("display", "");
                        }
                    }

                    var filteredData = [];
                    foreground
                        .style("display", function(d) {
                            var flag = actives.every(function(p, i) {
                                return extents[i][0] <= d.value.info[p] && d.value.info[p] <= extents[i][1];
                            });

                            if(flag) {
                                filteredData.push(d);
                            }

                            return flag ? "" : "none";
                        });

                    lineUpService.lineUp.setData(filteredData);
                }

                function setDimensions(_dimensions) {
                    dimensions = _dimensions;
                    drawGraph();
                }

                function switchIsHistogram() {
                    isHistogram = !isHistogram;
                    drawGraph();
                }
            }
        }
    }
}]);