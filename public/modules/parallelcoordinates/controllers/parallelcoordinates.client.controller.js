'use strict';

angular.module('parallelCoordinates').controller('parallelCoordinatesController', ['$scope', 'parallelCoordinatesService', 'pipService', 'lineUpService',
    function($scope, parallelCoordinatesService, pipService, lineUpService) {

        $scope.changeParallelCoordinatesControlParameters = changeParallelCoordinatesControlParameters;
        $scope.switchIsLegend = switchIsLegend;
        $scope.switchIsHistogram = switchIsHistogram;
        $scope.reset = redrawGraph;

        parallelCoordinatesService.getDimensions = getDimensions;

        pipService.onParallelCoordinatesDataChange($scope, function(data) {
            parallelCoordinatesService.loadData(data.mnList, data.selectedSource, data.selectedDest);
        });

        function getDimensions() {
            var dims = [];

            var dimDoms = $(".parallelCoordinates_dimensions");
            for(var i in dimDoms) {
                if(dimDoms[i].checked) {
                    dims.push(dimDoms[i].value);
                }
            }

            return dims;
        }

        function changeParallelCoordinatesControlParameters() {
            var dimensions = getDimensions();

            parallelCoordinatesService.setDimensions(dimensions);
        }

        function switchIsLegend() {
            parallelCoordinatesService.switchIsLegend();
        }

        function switchIsHistogram() {
            parallelCoordinatesService.switchIsHistogram();
        }

        function redrawGraph() {
            lineUpService.lineUp.setData(parallelCoordinatesService.data);

            parallelCoordinatesService.drawGraph();
        }
    }
]);