'use strict';

angular.module('radialGraph').controller('radialGraphController', ['$scope', 'radialGraphService', 'dataService', 'dbService', 'pipService',
    function($scope, radialGraphService, dataService, dbService, pipService) {


        $scope.switchIsLegend = switchIsLegend;
        $scope.changeControlParameters = changeControlParameters;

        radialGraphService.loadData = loadData;

        function switchIsLegend() {
            radialGraphService.switchIsLegend();
        }

        function changeControlParameters() {
            var radialGraph_hoursPerSector = $(".radialGraph_hoursPerSector")[0];
            radialGraphService.setRadialGraphHoursPerSector(radialGraph_hoursPerSector.value);

            var radialGraph_sortTypeDoms = $(".radialGraph_sortType");
            for(var i in radialGraph_sortTypeDoms) {
                if(radialGraph_sortTypeDoms[i].checked) {
                    radialGraphService.setRadialGraph_sortType(radialGraph_sortTypeDoms[i].value);

                    break;
                }
            }

            var radialGraph_maxDistance = $(".radialGraph_maxDistance")[0];
            radialGraphService.setRadialGraphMaxDistance(parseFloat(radialGraph_maxDistance.value));

            var radialGraph_sigma = $(".radialGraph_sigma")[0];
            radialGraphService.setRadialGraphSigmaOfGaussianSmooth(parseInt(radialGraph_sigma.value));

            radialGraphService.constructRadialGraph();
        }

        function loadData(id) {
            var dateList = dataService.getDateList();

            var name = '/radialgraph';
            var paras = {
                stationID: id,
                dateList: dateList
            };

            dbService.post(name, paras, function(data) {
                radialGraphService.data = data;

                pipService.emitRadialGraphDataChange(data);
            });
        }
    }
]);