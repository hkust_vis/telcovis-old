'use strict';

angular.module('map').controller('mapController', ['$scope', 'mapService', 'dataService', 'dbService', 'pipService',
    function($scope, mapService, dataService, dbService, pipService) {
        $scope.changeSourceControlParameters = changeSourceControlParameters;
        $scope.changeDestControlParameters = changeDestControlParameters;

        mapService.loadData = loadData;

        mapService.getCooccurrence = getCooccurrence;

        pipService.onDateListChange($scope, function() {
            mapService.loadData();
        });

        function loadData() {
            mapService.clear();

            var dateList = dataService.getDateList();

            var names = [
                "/map/info",
                '/map/heatmap/source',
                '/map/heatmap/destination'
            ];

            var parasList = [
                {
                },
                {
                    dateList: dateList
                }, {
                    dateList: dateList
                }

            ];

            dbService.posts(names, parasList, function(dataList) {
                mapService.data = {
                    info: dataList[0],
                    heatMap:  {
                        source: dataList[1],
                        dest: dataList[2]
                    },
                    map: {
                        source: undefined,
                        dest: undefined
                    },
                    d3Overlay: {
                        sel: {
                            source: undefined,
                            dest: undefined
                        },
                        proj: {
                            source: undefined,
                            dest: undefined
                        }
                    },
                    circles: {
                        source: undefined,
                        dest: undefined
                    }
                };

                pipService.emitMapDataChange(mapService.data);
            });
        }

        function getCooccurrence(selectedSource, selectedDest, callback) {
            var dateList = dataService.getDateList();
            var hours = dataService.getHours();
            var name = "/map/cooccurrence";

            var paras = {
                dateList: dateList,
                hours: hours,
                selectedSource: selectedSource,
                selectedDest: selectedDest
            };

            dbService.post(name, paras, function(data) {
                callback(data);
            });
        }

        function changeSourceControlParameters() {
            var circleSizeFilter = $(".source-circle-size-filter")[0];
            mapService.filterSourceCircles(+(circleSizeFilter.value));
        }

        function changeDestControlParameters() {
            var destCircleSizeIndexDoms = $(".dest_circle_size_index");
            for(var i in destCircleSizeIndexDoms) {
                if(destCircleSizeIndexDoms[i].checked) {
                    mapService.destCircleSizeIndex = destCircleSizeIndexDoms[i].value;

                    break;
                }
            }

            mapService.setDestCircleSize(mapService.destCircleSizeIndex);

            var circleSizeFilter = $(".dest-circle-size-filter")[0];
            mapService.filterDestCircles(+(circleSizeFilter.value));
        }
    }


]);