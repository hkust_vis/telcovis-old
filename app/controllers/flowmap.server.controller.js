'use strict';

var mongoose = require('mongoose');
var _ = require('lodash');
var func = require('./func.server.controller.js');

exports.getFlowMapData = getFlowMapData;

function getFlowMapData(req, res) {
    var paras = req.body;
    var gpsIDs = paras.gpsIDs;
    var dateList = paras.dateList;

    var mongoParas = generateMongoParas();

    func.getDataListMongo(mongoParas, function(dataList) {
        dataList = dataList.map(function(d) {
            return d[0];
        });

        var _dataList = [];
        var k = 0;
        for(var i = 0; i < gpsIDs.length; ++i) {
            var gpsData = [];
            _dataList.push(gpsData);
            for(var j = 0; j < dateList.length; ++j) {
                gpsData.push(dataList[k]);
                ++k;
            }
        }

        _dataList = combineDataOfDifferentDates(_dataList);

        res.send(_dataList);
        res.end();
    });


    function generateMongoParas() {
        var modelNames = [];
        var collectionNames = [];
        var findParas = [];

        for(var i = 0; i < gpsIDs.length; ++i) {
            for(var j = 0; j < dateList.length; ++j) {
                modelNames.push('flowMapData');
                collectionNames.push('flow');
                findParas.push({
                    date: dateList[j],
                    gpsID: +gpsIDs[i]
                });
            }
        }

        return {
            modelNames: modelNames,
            collectionNames: collectionNames,
            findParas: findParas
        }
    }

    function combineDataOfDifferentDates(_data) {
        var data = [];
        for(var i = 0; i < _data.length; ++i) {
            var gpsData = {
                gpsID: _data[i][0].gpsID,
                value: {}
            };
            for(var j = 0; j < _data[i].length; ++j) {
                var value = _data[i][j].value;
                for(var base in value) {
                    if(!gpsData.value.hasOwnProperty(base)) {
                        gpsData.value[base] = value[base];
                    } else {

                        for(var hour in value[base]) {
                            if(!gpsData.value[base].hasOwnProperty(hour)) {
                                gpsData.value[base][hour] = value[base][hour];
                            } else {
                                gpsData.value[base][hour] += value[base][hour];
                            }
                        }
                    }
                }
            }

            data.push(gpsData);
        }

        return data;
    }
}

