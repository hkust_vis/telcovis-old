'use strict';

var mongoose = require('mongoose');
var _ = require('lodash');
var func = require('./func.server.controller.js');

var processMongoResult = function processMongoResult(mongoResult) {
    var finalResult = _.indexBy(mongoResult, 'gpsID');

    return finalResult;
};

var getRadialGraphDataMongo = function(date, stationID, callback){
    var tuple = mongoose.model('radialGraphData');
    tuple.collection.collection.collectionName = 'date_' + date + '_treemap';

    tuple.find({stationID: +stationID}).exec(function(err, result){
        callback(result);
    });
};

exports.getRadialGraphData = function(req, res) {
    var paras = req.body;

    var stationID = paras.stationID;
    var dateList = paras.dateList;

    var mongoParas = generateMongoParas();

    func.getDataListMongo(mongoParas, function(dataList) {
        for(var i in dataList) {
            dataList[i] = processMongoResult(dataList[i]);
        }

        var data = combineDataOfDifferentDates(dataList);

        res.send(data);
        res.end();
    });

    function generateMongoParas() {
        var modelNames = [];
        var collectionNames = [];
        var findParas = [];

        for(var i = 0; i < dateList.length; ++i) {
            modelNames.push('radialGraphData');
            collectionNames.push('treemap');
            findParas.push({
                date: dateList[i],
                stationID: +stationID
            });
        }

        return {
            modelNames: modelNames,
            collectionNames: collectionNames,
            findParas: findParas
        }
    }

    function combineDataOfDifferentDates(_data) {
        var data = _data[0];
        for(var i = 1; i < _data.length; ++i) {
            var gpsDataSet = _data[i];
            for(var j in gpsDataSet) {
                var _gpsData = gpsDataSet[j];
                if(!data.hasOwnProperty(j)) {
                    data[j] = _gpsData;
                } else {
                    var persons = data[j].value.persons;
                    var _persons = _gpsData.value.persons;
                    for(var hour in _persons) {
                        if(!persons.hasOwnProperty(hour)) {
                            persons[hour] = _persons[hour];
                        } else {
                            persons[hour] += _persons[hour];
                        }
                    }
                }
            }
        }

        return data;
    }
};