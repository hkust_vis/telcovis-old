'use strict';
var mongoose = require('mongoose');

exports.getDataMongo = getDataMongo;
exports.getDataListMongo = getDataListMongo;

function getDataMongo(paras, callback) {
    var modelName = paras.modelName;
    var collectionName = paras.collectionName;
    var findPara = paras.findPara;
    var limit = paras.limit;

    var tuple = mongoose.model(modelName);
    tuple.collection.collection.collectionName = collectionName;

    var execFunc = tuple.find(findPara);
    if(limit) {
        execFunc = execFunc.limit(limit);
    }

    execFunc.exec(function(err, result) {
        callback(result);
    });
}

function getDataListMongo(paras, callback) {
    var modelNames = paras.modelNames;
    var collectionNames = paras.collectionNames;
    var findParas = paras.findParas;
    var limits = paras.limits;

    var dataList = [];
    var index = 0;

    var _paras = {
        modelName: modelNames[index],
        collectionName: collectionNames[index],
        findPara: findParas?findParas[index]:{},
        limit: limits?limits[index]:undefined
    };

    getDataMongo(_paras, _callback);

    function _callback(data) {
        dataList.push(data);

        ++index;
        if(index == modelNames.length) {
            callback(dataList);
        } else {
            _paras = {
                modelName: modelNames[index],
                collectionName: collectionNames[index],
                findPara: findParas?findParas[index]:{},
                limit: limits?limits[index]:undefined
            };

            getDataMongo(_paras, _callback);
        }
    }
}