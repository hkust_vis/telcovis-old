'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = mongoose.Schema.Types.Mixed;

var lineUpDataSchema = new Schema({
    m_n: String,
    order: Number,
    value: Mixed
});

mongoose.model('lineUpData', lineUpDataSchema);