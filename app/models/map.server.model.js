'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = mongoose.Schema.Types.Mixed;

var sourceMapSchema = new Schema({
    gpsID: Number,
    value: Mixed
});

var destMapSchema = new Schema({
    stationID: Number,
    value: Mixed
});

var mapInfoSchema = new Schema({
    value: Mixed
});

var heatMapSchema = new Schema({
    date: String,
    value: Mixed
});

var cooccurrenceSourceSchema = new Schema({
    date: Number,
    value: Mixed,
    hour: Number,
    gpsID: Number
});

var cooccurrenceDestSchema = new Schema({
    date: Number,
    value: Mixed,
    hour: Number,
    gpsID: Number,
    stationID: Number
});

mongoose.model('sourceMap', sourceMapSchema);
mongoose.model('destMap', destMapSchema);

mongoose.model('mapInfo', mapInfoSchema);
mongoose.model('heatMap', heatMapSchema);

mongoose.model('cooccurrenceSource', cooccurrenceSourceSchema);
mongoose.model('cooccurrenceDest', cooccurrenceDestSchema);