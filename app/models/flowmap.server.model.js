'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = mongoose.Schema.Types.Mixed;

var flowMapDataSchema = new Schema({
    gpsID: Number,
    value: Mixed
});

mongoose.model('flowMapData', flowMapDataSchema);