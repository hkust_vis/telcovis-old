'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = mongoose.Schema.Types.Mixed;

var radialGraphDataSchema = new Schema({
    stationID: Number,
    gpsID: Number,
    value: Mixed
});

mongoose.model('radialGraphData', radialGraphDataSchema);