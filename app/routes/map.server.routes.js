'use strict';

module.exports = function(app) {
    var map = require('../../app/controllers/map.server.controller');

    app.route('/map/source/all').post(map.getSourceMap);
    app.route('/map/destination/all').post(map.getDestMap);

    app.route('/map/info').post(map.getMapInfo);

    app.route('/map/heatmap/source').post(map.getHeatMapSource);
    app.route('/map/heatmap/destination').post(map.getHeatMapDest);

    app.route('/map/cooccurrence').post(map.getCooccurrence);
};
