'use strict';

module.exports = function(app) {
    var matrix = require('../../app/controllers/matrix.server.controller');

    app.route('/matrix/day').post(matrix.getMatrixData);
    app.route('/matrix/hour').post(matrix.getMatrixDataHours);
    app.route('/matrix/test').post(matrix.testCellDataDeeply);
};
