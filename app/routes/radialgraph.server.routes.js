'use strict';

module.exports = function(app) {
    var radialGraph = require('../../app/controllers/radialgraph.server.controller');

    app.route('/radialgraph').post(radialGraph.getRadialGraphData);
};
