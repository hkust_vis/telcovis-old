'use strict';

module.exports = function(app) {
    var lineUp = require('../../app/controllers/lineup.server.controller');

    app.route('/lineup/filter').post(lineUp.getLineUpDataFiltered);
};