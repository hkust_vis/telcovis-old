'use strict';

module.exports = function(app) {
    var flowMap = require('../../app/controllers/flowmap.server.controller');

    app.route('/flowmap').post(flowMap.getFlowMapData);
};