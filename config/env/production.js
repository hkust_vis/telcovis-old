'use strict';

module.exports = {
	db: {
		uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/telco',
		options: {
			user: '',
			pass: ''
		}
	},
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'combined',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			stream: 'access.log'
		}
	},
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
                'public/lib/leaflet/dist/leaflet.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',

                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/jquery-ui/jquery-ui.min.js',

                'public/lib/underscore/underscore-min.js',
                'public/lib/underscore/underscore.math.js',

                'public/lib/bootstrap/dist/js/bootstrap.min.js',

                'public/lib/d3/d3.min.js',
                'public/lib/d3/d3.geom.nhull.js',
                'public/lib/d3.layout.voronoiTreemap.js',

                'public/lib/leaflet/dist/leaflet.js',
                'public/lib/leaflet/dist/leaflet-heat.js',
                'public/lib/leaflet/dist/L.D3SvgOverlay.min.js',

                'public/lib/circle_packer_mover.js',
                'public/lib/numeric-1.2.6.min.js',
                'public/lib/mds.js',
                'public/lib/power.js',
                'public/lib/horizon.js',

                'public/lib/lineup/lineup_datastructure.js',
                'public/lib/lineup/lineup_storage.js',
                'public/lib/lineup/lineup.js',
                'public/lib/lineup/lineup_tableheader.js',
                'public/lib/lineup/lineup_tablebody.js',
                'public/lib/lineup/lineup_gui_helper.js',
                'public/lib/lineup/lineup_mappingeditor.js',

                'public/lib/RadialGraph.js',
//                'public/lib/VoronoiTreemap.js'

                'public/lib/flowmap/FlowMap.js',
                'public/lib/flowmap/NormalFlowMap.js',
                'public/lib/flowmap/PointsView.js',
                'public/lib/flowmap/RatioBarView.js',
                'public/lib/flowmap/VoronoiRegionView.js'
			]
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: '/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	}
};
